package com.rsl.onlineexam.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.onlineexam.R;
import com.rsl.onlineexam.model.PracticeQuestions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 11/19/2015.
 */
public class TestPageActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener
{
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private Toolbar mToolbar;
    private TextView timer_count;
    private ConnectionDetector cd;
    private Button btn_prev,btn_next;
    HorizontalPagerAdapter mCustomPagerAdapter;
    ArrayList<HashMap<String, String>> list_data = null;
    ArrayList<PracticeQuestions> practiceQusetions_data;
    ViewPager mViewPager;
    LayoutInflater mLayoutInflater;
    LinearLayout myGallery;
    int count=1;
    ArrayList<HashMap<String, String>> data = null;
    private long startTime = 0L;
    private Handler customHandler = new Handler();
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;
    CountDownTimer countDownTimer;          // built in android class CountDownTimer
    long totalTimeCountInMilliseconds;      // total count down time in milliseconds
    long timeBlinkInMilliseconds;           // start time of start blinking
    boolean blink;
    private JSONObject objrepot;
    private boolean testFlag=false;
    TextView title;
    //int time =0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testpage);
        sp = getSharedPreferences("ObjectiveExam", 0);
        editor = sp.edit();
        list_data = new ArrayList<HashMap<String,String>>();
        data = new ArrayList<HashMap<String,String>>();
        mLayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
         title = (TextView)mToolbar.findViewById(R.id.txt_title);
        title.setText(getIntent().getStringExtra("test_name"));
        //time = Integer.parseInt(getIntent().getStringExtra("time"));
        timer_count = (TextView) mToolbar.findViewById(R.id.toolbar_timer);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final int abTitleId = getResources().getIdentifier("action_bar_title", "id", "android");
        title.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Do something
                clickOnTitle();
            }
        });
        cd = new ConnectionDetector(getApplicationContext());
        practiceQusetions_data = new ArrayList<PracticeQuestions>();
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mCustomPagerAdapter);
        mViewPager.setOnPageChangeListener(this);
        myGallery = (LinearLayout)findViewById(R.id.mygallery);
        //totalTimeCountInMilliseconds = time * 60 * 1000;
        //timeBlinkInMilliseconds = 30 * 1000;
        if (cd.isConnectingToInternet())
        {
            getPracticeQuesrions(sp.getString("SubjectId", ""),sp.getString("test_id",""));
        } else {
            cd.showAlertDialog(TestPageActivity.this, "No Internet Connection",
                    "You don't have internet connection.", false);
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        title.setText(getIntent().getStringExtra("test_name"));
    }
    View insertImage(final int path){
        LinearLayout layout = new LinearLayout(getApplicationContext());
        LinearLayout.LayoutParams layoutParams= new LinearLayout.LayoutParams( LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(5,5,5,5);
        //layoutParams.setPadd
        layout.setLayoutParams(layoutParams);
        layout.setBackgroundResource(R.drawable.round);
        TextView qno = new TextView(getApplicationContext());
        LinearLayout.LayoutParams layoutParams1= new LinearLayout.LayoutParams( LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(5,5,5,5);
        qno.setLayoutParams(new ViewGroup.LayoutParams(layoutParams1));
        qno.setTextColor(Color.WHITE);
        qno.setText("Q" + (path + 1));
        qno.setPadding(10,10,10,10);
        layout.addView(qno);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(path);
            }
        });
        return layout;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }
    @Override
    public void onPageSelected(int position) {
        count=position;
        count++;
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private Runnable updateTimerThread = new Runnable() {

        public void run() {

            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
            updatedTime = timeSwapBuff + timeInMilliseconds;
            int secs = (int) (updatedTime / 1000);
            int mins = secs / 60;
            secs = secs % 60;
            //    int milliseconds = (int) (updatedTime % 1000);
            timer_count.setText("" + mins + ":"
                    + String.format("%02d", secs));
            customHandler.postDelayed(this, 0);
        }
    };
    class HorizontalPagerAdapter extends PagerAdapter
    {
        public List<PracticeQuestions> practiceQusetionsList;
        Context mContext;
        public HorizontalPagerAdapter(Context context,List<PracticeQuestions> practiceQusetionsList) {
            mContext = context;
            this.practiceQusetionsList = practiceQusetionsList;
            Log.e("StudentAdapter", "" + practiceQusetionsList);
        }
        public void add(HashMap<String, String> data) {
            Log.e("StudentAdapter", "add");
            list_data.add(data);
            notifyDataSetChanged();
        }
        public PracticeQuestions getPositionData(int pos){
            return practiceQusetionsList.get(pos);
        }

        public List<PracticeQuestions> getUpdatedData(){
            return practiceQusetionsList;
        }

        @Override
        public void destroyItem(View container, int position, Object object) {
            // TODO Auto-generated method stub
            ((ViewPager) container).removeView((View) object);
        }
        @Override
        public int getCount() {
            return practiceQusetionsList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view==object;
        }
        @Override
        public Object instantiateItem(ViewGroup container,final int position) {
            View itemView = mLayoutInflater.inflate(R.layout.pager_item,
                    container, false);
            TextView question = (TextView)itemView.findViewById(R.id.txt_question);
            TextView option1 = (TextView)itemView.findViewById(R.id.txt_ans_a);
            TextView option2 = (TextView)itemView.findViewById(R.id.txt_ans_b);
            TextView option3 = (TextView)itemView.findViewById(R.id.txt_ans_c);
            TextView option4 = (TextView)itemView.findViewById(R.id.txt_ans_d);
            question.setText("Q"+(position+1)+": "+practiceQusetionsList.get(position).getQuestions());
            option1.setText(practiceQusetionsList.get(position).getOption1());
            option2.setText(practiceQusetionsList.get(position).getOption2());
            option3.setText(practiceQusetionsList.get(position).getOption3());
            option4.setText(practiceQusetionsList.get(position).getOption4());
            final Button btnA = (Button) itemView.findViewById(R.id.btn_option_a);
            final Button btnB = (Button) itemView.findViewById(R.id.btn_option_b);
            final Button btnC = (Button) itemView.findViewById(R.id.btn_option_c);
            final Button btnD = (Button) itemView.findViewById(R.id.btn_option_d);
            final LinearLayout btnOption_A = (LinearLayout) itemView.findViewById(R.id.buttonPanela);
            final LinearLayout btnOption_B = (LinearLayout) itemView.findViewById(R.id.buttonPanelb);
            final LinearLayout btnOption_C = (LinearLayout) itemView.findViewById(R.id.buttonPanelc);
            final LinearLayout btnOption_D = (LinearLayout) itemView.findViewById(R.id.buttonPaneld);
            String selectedOption = practiceQusetionsList.get(position).getSelectedOption();
            if(selectedOption.equals("A")){
                btnA.setBackgroundResource(R.color.greenclr);
                btnD.setBackgroundResource(R.color.grayclr);
                btnC.setBackgroundResource(R.color.grayclr);
                btnB.setBackgroundResource(R.color.grayclr);
                practiceQusetionsList.get(position).setSelectedOption("A");
            }else if(selectedOption.equals("B")){
                btnB.setBackgroundResource(R.color.greenclr);
                btnA.setBackgroundResource(R.color.grayclr);
                btnC.setBackgroundResource(R.color.grayclr);
                btnD.setBackgroundResource(R.color.grayclr);
                practiceQusetionsList.get(position).setSelectedOption("B");
            }else if(selectedOption.equals("C")){
                btnC.setBackgroundResource(R.color.greenclr);
                btnB.setBackgroundResource(R.color.grayclr);
                btnA.setBackgroundResource(R.color.grayclr);
                btnD.setBackgroundResource(R.color.grayclr);
                practiceQusetionsList.get(position).setSelectedOption("C");
            }else if(selectedOption.equals("D")){
                btnD.setBackgroundResource(R.color.greenclr);
                btnC.setBackgroundResource(R.color.grayclr);
                btnB.setBackgroundResource(R.color.grayclr);
                btnA.setBackgroundResource(R.color.grayclr);
                practiceQusetionsList.get(position).setSelectedOption("D");
            }else{
                btnD.setBackgroundResource(R.color.grayclr);
                btnC.setBackgroundResource(R.color.grayclr);
                btnB.setBackgroundResource(R.color.grayclr);
                btnA.setBackgroundResource(R.color.grayclr);
            }
            View.OnClickListener v1 = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    testFlag = true;
                    btnA.setBackgroundResource(R.color.greenclr);
                    btnD.setBackgroundResource(R.color.grayclr);
                    btnC.setBackgroundResource(R.color.grayclr);
                    btnB.setBackgroundResource(R.color.grayclr);
                    practiceQusetionsList.get(position).setSelectedOption("A");
                    practiceQusetionsList.get(position).setSelectedQuestionNo(String.valueOf(position+1));
                    practiceQusetionsList.get(position).setSelectedOptionId(practiceQusetionsList.get(position).getOptionId1());
                    practiceQusetionsList.get(position).setSelectedOptionAns(practiceQusetionsList.get(position).getOption1());
                    Log.e("Option A", "" + practiceQusetionsList.get(position).getOptionId1());
                }
            };
            View.OnClickListener v2 = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    testFlag = true;
                    btnB.setBackgroundResource(R.color.greenclr);
                    btnA.setBackgroundResource(R.color.grayclr);
                    btnC.setBackgroundResource(R.color.grayclr);
                    btnD.setBackgroundResource(R.color.grayclr);
                    Log.e("Option B", "" + practiceQusetionsList.get(position).getOptionId2());
                    practiceQusetionsList.get(position).setSelectedOptionId(practiceQusetionsList.get(position).getOptionId2());
                    practiceQusetionsList.get(position).setSelectedOptionAns(practiceQusetionsList.get(position).getOption2());
                    practiceQusetionsList.get(position).setSelectedOption("B");
                    practiceQusetionsList.get(position).setSelectedQuestionNo(String.valueOf(position + 1));
                }
            };
            View.OnClickListener v3 = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    testFlag = true;
                    btnC.setBackgroundResource(R.color.greenclr);
                    btnB.setBackgroundResource(R.color.grayclr);
                    btnA.setBackgroundResource(R.color.grayclr);
                    btnD.setBackgroundResource(R.color.grayclr);
                    Log.e("Option C", "" + practiceQusetionsList.get(position).getOptionId3());
                    practiceQusetionsList.get(position).setSelectedOptionId(practiceQusetionsList.get(position).getOptionId3());
                    practiceQusetionsList.get(position).setSelectedOptionAns(practiceQusetionsList.get(position).getOption3());
                    practiceQusetionsList.get(position).setSelectedOption("C");
                    practiceQusetionsList.get(position).setSelectedQuestionNo(String.valueOf(position + 1));

                }
            };
            View.OnClickListener v4 = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    testFlag = true;
                    btnD.setBackgroundResource(R.color.greenclr);
                    btnC.setBackgroundResource(R.color.grayclr);
                    btnB.setBackgroundResource(R.color.grayclr);
                    btnA.setBackgroundResource(R.color.grayclr);
                    practiceQusetionsList.get(position).setSelectedOptionId(practiceQusetionsList.get(position).getOptionId4());
                    practiceQusetionsList.get(position).setSelectedOptionAns(practiceQusetionsList.get(position).getOption4());
                    practiceQusetionsList.get(position).setSelectedOption("D");
                    practiceQusetionsList.get(position).setSelectedQuestionNo(String.valueOf(position + 1));
                    Log.e("Option D", "" + practiceQusetionsList.get(position).getOptionId4());
                }
            };
            btnOption_A.setOnClickListener(v1);
            btnA.setOnClickListener(v1);
            btnOption_B.setOnClickListener(v2);
            btnB.setOnClickListener(v2);
            btnOption_C.setOnClickListener(v3);
            btnC.setOnClickListener(v3);
            btnOption_D.setOnClickListener(v4);
            btnD.setOnClickListener(v4);
            container.addView(itemView);
            return itemView;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
// Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_submit, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_submit) {
            AlertDialog.Builder alert = new AlertDialog.Builder(TestPageActivity.this);
            alert.setTitle("Confirm");
            alert.setIcon(R.mipmap.ic_launcher);
            alert.setMessage("Are you sure want to finish this test?");
            alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //countDownTimer.cancel();
                    timeSwapBuff += timeInMilliseconds;
                    customHandler.removeCallbacks(updateTimerThread);
                    Log.e("action_submit", "" + data);
                    JSONObject obj = new JSONObject();
                    objrepot = new JSONObject();
                    List<PracticeQuestions> AllData = mCustomPagerAdapter.getUpdatedData();
                    JSONArray req = new JSONArray();
                    try {
                        //JSONArray req = new JSONArray();
                        JSONArray reqRepot = new JSONArray();
                        for (int i = 0; i < AllData.size(); i++) {
                            JSONObject obj1 = new JSONObject();
                            JSONObject obj2 = new JSONObject();
                            PracticeQuestions d = AllData.get(i);
                            obj1.put("topic_id", d.getTopicId());
                            obj1.put("question_id", d.getQuestionId());
                            obj1.put("option_id", d.getSelectedOptionId());

                            obj2.put("question", d.getQuestions());
                            obj2.put("question_id", d.getQuestionId());
                            obj2.put("Explanation", d.getExplation());
                            obj2.put("Ans_option", d.getSelectedOptionAns());
                            obj2.put("CorrectAns", d.getCurrectans());
                            obj2.put("Check", d.getSelectedOption());
                            obj2.put("option_id", d.getSelectedOptionId());
                            obj2.put("questionNo",d.getSelectedQuestionNo());
                            if (!d.getSelectedOptionId().equals("")) {
                                req.put(obj1);
                                reqRepot.put(obj2);
                            }
                        }
                        objrepot.put("data", reqRepot);
                        obj.put("Question", req);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.e("action_submitRepot", "" + objrepot);
                    Log.e("action_submit", "" + obj);
                    //web service call for inserting practice test data
                    if (testFlag) {
                        //web service call for inserting practice test data
                        if (getIntent().getStringExtra("done").equals("true")) {
                            Intent submit = new Intent(TestPageActivity.this, ReportPageActivity.class);
                            submit.putExtra("jsonArray", objrepot.toString());
                            submit.putExtra("activity", "TestPage");
                            startActivity(submit);
                            finish();
                        } else {
                            insertingPracticeTestData(sp.getString("SubjectId", ""), sp.getString("students_id", ""), sp.getString("test_id", ""), req);
                        }
                        }else
                        {
                            Toast.makeText(TestPageActivity.this, "No Questions Attempted", Toast.LENGTH_LONG).show();
                            finish();
                        }
                }
            });
            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alert.create().show();
            return true;
        }
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
   private  void clickOnTitle(){
       AlertDialog.Builder alert = new AlertDialog.Builder(TestPageActivity.this);
       alert.setTitle("Test Details");
       alert.setIcon(R.mipmap.ic_launcher);
       alert.setMessage("Subject name : " + sp.getString("SubjectName", "") + "\n\nTest Name : " + getIntent().getStringExtra("test_name") + "\n\nNo of Questions : " + practiceQusetions_data.size());
       alert.setNeutralButton("ok", new DialogInterface.OnClickListener() {

           @Override
           public void onClick(DialogInterface dialog, int which) {
               dialog.dismiss();
           }
       });
       alert.create().show();
   }
    @Override
    public void onBackPressed() {
        AlertDialog.Builder alert = new AlertDialog.Builder(TestPageActivity.this);
        alert.setTitle("Confirm");
        alert.setIcon(R.mipmap.ic_launcher);
        alert.setMessage("Are you sure want to go back?");
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Intent back = new Intent(TestPageActivity.this,MainActivity.class);
                //startActivity(back);
                //countDownTimer.cancel();
                timeSwapBuff += timeInMilliseconds;
                customHandler.removeCallbacks(updateTimerThread);
                MainActivity.flag=true;
                finish();
            }
        });
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert.create().show();
    }
private  void getPracticeQuesrions(final String subId,final String topic)
{
    final ProgressDialog dialog = new ProgressDialog(TestPageActivity.this,R.style.MyThemeHoloDialog);
    //dialog.setTitle("Loading...");
    dialog.setCancelable(false);
    dialog.show();
    final RequestQueue queue = Volley.newRequestQueue(TestPageActivity.this);
    String requestURL =getResources().getString(R.string.web_service_url)+"test_question.php";
    JSONObject obj = new JSONObject();
    try {
        obj.put("test_id", topic);
        obj.put("sub_id", subId);
    } catch (JSONException e) {
        e.printStackTrace();
    }
    Log.e("JSONObject ", "" + obj.toString());
    StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
        @Override
        public void onResponse(String s) {
            if (s != null) {
                Log.e("JSONObject ", "" + s.toString());
                if (!s.startsWith("null")) {
                    try {
                        JSONObject jObj = new JSONObject(s);
                        if (jObj.length() > 0) {
                            if (jObj.getString("result").equals("success"))
                            {
                                for(int i = 0;i<jObj.getJSONArray("data").length(); i++)
                                {
                                    myGallery.addView(insertImage(i));
                                }
                                if (jObj.getJSONArray("data").length() > 0)
                                {
                                    //countDownTimer.start();
                                    int id = jObj.getJSONArray("data").length();
                                    Log.e("Iddddddddddddd",""+id);
                                    startTime = SystemClock.uptimeMillis();
                                    customHandler.postDelayed(updateTimerThread, 0);
                                    for (int i = 0; i < jObj.getJSONArray("data").length(); i++)
                                    {
                                        JSONObject jobj_Data = jObj.getJSONArray("data").getJSONObject(i);
                                        PracticeQuestions data1 = new PracticeQuestions();
                                        HashMap<String,String> map = new HashMap<String,String>();
                                        data1.setQuestions(jobj_Data.getString("question"));
                                        data1.setCurrectans(jobj_Data.getString("currectans"));
                                        data1.setQuestionId(jobj_Data.getString("question id"));
                                        data1.setTopicId(jobj_Data.getString("topic id"));
                                        data1.setExplation(jobj_Data.getString("explanation"));
                                        map.put("question", jobj_Data.getString("question"));
                                        map.put("currectans", jobj_Data.getString("currectans"));
                                        map.put("question id", jobj_Data.getString("question id"));
                                        map.put("topic id", jobj_Data.getString("topic id"));
                                        map.put("explanation", jobj_Data.getString("explanation"));
                                        if (jobj_Data.getJSONArray("answer").length()>0)
                                        {
                                            for (int b = 0; b < jobj_Data.getJSONArray("answer").length(); b++)
                                            {
                                                JSONObject answer = jobj_Data.getJSONArray("answer").getJSONObject(b);
                                                if(b==0){
                                                    data1.setOptionId1(answer.getString("optionid"));
                                                    data1.setOption1(answer.getString("option"));
                                                }else if(b==1){
                                                    data1.setOptionId2(answer.getString("optionid"));
                                                    data1.setOption2(answer.getString("option"));
                                                }else if(b==2){
                                                    data1.setOptionId3(answer.getString("optionid"));
                                                    data1.setOption3(answer.getString("option"));
                                                }else if(b==3){
                                                    data1.setOptionId4(answer.getString("optionid"));
                                                    data1.setOption4(answer.getString("option"));
                                                }
                                                map.put("option_id"+b,answer.getString("optionid"));
                                                map.put("ans_option"+b,answer.getString("option"));
                                            }
                                        }
                                        practiceQusetions_data.add(data1);
                                        mCustomPagerAdapter = new HorizontalPagerAdapter(TestPageActivity.this,practiceQusetions_data);
                                        mViewPager.setAdapter(mCustomPagerAdapter);
                                        //mCustomPagerAdapter.add(map);
                                    }
                                    Log.e("practiceQusetionssize",""+ practiceQusetions_data.size());
                                    data = new ArrayList<HashMap<String,String>>(practiceQusetions_data.size());
                                    Log.e("list_data", "" + list_data);
                                }else {
                                    alertDialog("No Questions Available In This Test!");
                                    MainActivity.flag=true;
                                }
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                            } else if (jObj.getString("result")
                                    .equals("failed")) {
                                dialog.dismiss();
                                //alertDialog("In valid Email id and password");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        dialog.dismiss();
                    }
                } else {
                    dialog.dismiss();
                }
            } else {
                alertDialog("Please try again");
                dialog.dismiss();
            }
        }
    }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError volleyError) {

        }
    }){
        @Override
        protected Map<String, String> getParams () {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("test_id", topic);
            params.put("sub_id", subId);
            return params;
        }
        @Override
        public Map<String, String> getHeaders ()throws AuthFailureError {
            Map<String, String> params = new HashMap<String, String>();
            params.put("Content-Type", "application/x-www-form-urlencoded");
            return params;
        }
    };
    queue.add(stringRequest);
}
    private void alertDialog(String errer) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                getSupportActionBar().getThemedContext());
        alertDialog.setTitle("OnlineExam");
        alertDialog.setMessage(errer);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        finish();
                    }
                });
        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                TestPageActivity.this.finish();
            }
        });
        alertDialog.show();
    }
    private  void insertingPracticeTestData(final String sub_id,final String students_id,final String test_id ,final JSONArray data)
    {
        final ProgressDialog dialog = new ProgressDialog(TestPageActivity.this,R.style.MyThemeHoloDialog);
        //dialog.setTitle("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(TestPageActivity.this);
        String requestURL =getResources().getString(R.string.web_service_url)+"insert_test_result.php";
        JSONObject obj = new JSONObject();
        try {
            obj.put("sub_id", sub_id);
            obj.put("students_id", students_id);
            obj.put("question",data);
            obj.put("test_id",test_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST,requestURL.replaceAll(" ","%20"),new Response.Listener<String>()
        {
            @Override
            public void onResponse(String s)
            {
                Log.e("onResponse ",s);
                if (s != null) {
                    if (!s.startsWith("null")) {
                        try {
                            JSONObject jObj = new JSONObject(s);
                            if (jObj.length() > 0) {
                                if (jObj.getString("result").equals("success"))
                                {
                                    Intent submit = new Intent(TestPageActivity.this, ReportPageActivity.class);
                                    submit.putExtra("jsonArray",objrepot.toString());
                                    submit.putExtra("activity", "TestPage");
                                    startActivity(submit);
                                    finish();
                                    if (dialog.isShowing()) {
                                        dialog.dismiss();
                                    }
                                } else
                                    dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    } else {
                        dialog.dismiss();
                    }
                } else {
                    alertDialog("Please try again");
                    dialog.dismiss();
                }
            }
        },new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
            }
        }){
            @Override
            protected Map<String, String> getParams () {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("sub_id", sub_id);
                params.put("student_id", students_id);
                params.put("question",data.toString());
                params.put("test_id",test_id);
                Log.e("JSONObject ", "" + params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders ()throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(stringRequest);
    }
}