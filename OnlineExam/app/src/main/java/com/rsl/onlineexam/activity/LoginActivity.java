package com.rsl.onlineexam.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.onlineexam.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements OnClickListener {
    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;
    private Button btn_login, btn_reg;
    SharedPreferences sp;
    MCrypt mc = new MCrypt();
    SharedPreferences.Editor editor;
    private ConnectionDetector cd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sp = getSharedPreferences("ObjectiveExam", 0);
        editor = sp.edit();
        // Set up the login form.
        mEmailView = (EditText) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_GO){
                    //perfrom your action
                    //Handling "Go" Button to move to next input field
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    // Store values at the time of the login attempt.
                    String email = mEmailView.getText().toString().trim();
                    String password = mPasswordView.getText().toString().trim();
                    if (email.length() <= 0) {
                        alertDialog("Please enter Email id");
                    }  else if (!validateEmail((email))) {
                        alertDialog("Please enter valid Email id");
                    } else if (password.length() <= 0) {
                        alertDialog("Please enter Password");
                    } else {
                        Login(email, password);
                    }
                }
                return false;
            }
        });
        btn_login = (Button) findViewById(R.id.email_sign_in_button);
        btn_reg = (Button) findViewById(R.id.email_regi_in_button);
        btn_login.setOnClickListener(this);
        btn_reg.setOnClickListener(this);
        cd = new ConnectionDetector(getApplicationContext());
        if (cd.isConnectingToInternet()) {
            if (sp.getString("students_id", "no user").equals("no user")) {
                Log.v("if Spash_Screen", "if Spash_Screen");
            } else {
                Log.e("else Spash_Screen", "else Spash_Screen");
                Intent i = new Intent(LoginActivity.this,
                        MainActivity.class);
                startActivity(i);
                finish();
            }
        } else {
            cd.showAlertDialog(LoginActivity.this, "No Internet Connection",
                    "You don't have internet connection.", false);
        }
    }
    // Email Validation
    public boolean validateEmail(String email) {
        Pattern pattern;
        Matcher matcher;
        String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() < 3;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.email_sign_in_button:
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                // Store values at the time of the login attempt.
                String email = mEmailView.getText().toString().trim();
                String password = mPasswordView.getText().toString().trim();
                if (!cd.isConnectingToInternet()) {
                    cd.showAlertDialog(LoginActivity.this, "No Internet Connection",
                            "You don't have internet connection.", false);
                }else if (email.length() <= 0) {
                   alertDialog("Please enter Email id");
                }  else if (!validateEmail((email))) {
                    alertDialog("Please enter valid Email id");
                }else if (password.length() <= 0) {
                   alertDialog("Please enter Password");
                } else {
                    Login(email, password);
                }
                break;
            case R.id.email_regi_in_button:
                Intent regi = new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(regi);
                finish();
                break;
            default:
                break;
        }
    }

    private  void Login(final String email,final String password)
    {
        final ProgressDialog dialog = new ProgressDialog(LoginActivity.this,R.style.MyThemeHoloDialog);
        //dialog.setTitle("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
        String requestURL =getResources().getString(R.string.web_service_url)+"User_login.php";
        JSONObject obj = new JSONObject();
        try {
            obj.put("user_email", email);
            try {
                obj.put("user_pass", MCrypt
                        .bytesToHex(mc.encrypt(password)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("JSONObject ", "" + obj.toString());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                if (s != null) {
                    Log.e("JSONObject ",s);
                    if (!s.startsWith("null")) {
                        try {
                            JSONObject jObj = new JSONObject(s);
                            if (jObj.length() > 0) {
                                if (jObj.getString("result").equals("success")) {
                                    editor.putString("students_id",
                                            jObj.getString("students_id"));
                                    editor.putString("students_email",
                                            jObj.getString("students_email"));
                                    editor.putString("student_name",jObj.getString("students_name"));
                                    editor.putString("student_image", jObj.getString("students_image"));
                                    editor.putString("school_name", jObj.getString("school_name"));
                                    editor.commit();
                                   // alertDialog("Loging Success");
                                    //call Select Subject  Activity
                                    Intent in = new Intent(LoginActivity.this,SelectSubjectActivity.class);
                                    startActivity(in);
                                    finish();
                                    if (dialog.isShowing()) {
                                        dialog.dismiss();
                                    }
                                } else if (jObj.getString("result")
                                        .equals("failed")) {
                                    dialog.dismiss();
                                    alertDialog("incorrect email or password!!");
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    } else {
                        dialog.dismiss();
                    }
                } else {
                    alertDialog("Please try again");
                    dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
            }
        }){
            @Override
            protected Map<String, String> getParams () {
                HashMap<String, String> params = new HashMap<String, String>();
                    params.put("user_email", email);
                    try {
                        params.put("user_pass", MCrypt
                                .bytesToHex(mc.encrypt(password)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                return params;
            }
            @Override
            public Map<String, String> getHeaders ()throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(stringRequest);
    }

    private void alertDialog(String errer) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                LoginActivity.this);
        alertDialog.setTitle("OnlineExam");
        alertDialog.setMessage(errer);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                    }
                });
        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                LoginActivity.this.finish();
            }
        });
        alertDialog.show();
    }
}