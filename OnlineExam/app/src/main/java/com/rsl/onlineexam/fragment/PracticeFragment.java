package com.rsl.onlineexam.fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.rsl.onlineexam.R;
import com.rsl.onlineexam.activity.ConnectionDetector;
import com.rsl.onlineexam.activity.MainActivity;
import com.rsl.onlineexam.activity.PracticePageActivity;


public class PracticeFragment extends Fragment
{
    private Button begin_practice;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private ConnectionDetector cd;
    public PracticeFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView= inflater.inflate(R.layout.fragment_practice, container, false);
        sp = getActivity().getSharedPreferences("ObjectiveExam", 0);
        editor = sp.edit();
        MainActivity.mToolbar.setVisibility(View.VISIBLE);
        cd = new ConnectionDetector(getActivity().getApplicationContext());
        begin_practice = (Button)rootView.findViewById(R.id.btn_begin_practice);
        begin_practice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {
                    Intent begin = new Intent(getActivity(),PracticePageActivity.class);
                    startActivity(begin);
                } else {
                    cd.showAlertDialog(getActivity(), "No Internet Connection",
                            "You don't have internet connection.", false);
                }
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        //MainActivity.mToolbar.setTitle(sp.getString("SubjectName", ""));
        MainActivity.mTitle.setText(sp.getString("SubjectName", ""));
        MainActivity.currentF = "home";
//        MainActivity.item.setVisible(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }
    @Override
    public void onDetach() {
        super.onDetach();

    }
}
