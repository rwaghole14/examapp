package com.rsl.onlineexam.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.camera.CropImageIntentBuilder;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.onlineexam.R;
import com.rsl.onlineexam.model.RoundImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 10/31/2015.
 */
public class RegisterActivity extends AppCompatActivity implements OnClickListener {
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    Button reg;
    String email, password, con_password,schoolId,name;
    EditText isEmEditText, isPEditText, isCnEdtiText,isNaEditText;
    Spinner school;
    MCrypt mc = new MCrypt();
    ImageView imageView1;
    private Toolbar mToolbar;
    RoundImage roundedImage;
    HashMap<String, String> data = null;
    ArrayAdapter<String> adapter;
    private ArrayList<HashMap<String, String>> list_data;
    ArrayList<String> schoolname_Array = new ArrayList<String>();
    private ConnectionDetector cd;
    private static final int REQUEST_CROP_PICTURE = 786;
    String encodedImage = "";
    private static final int PICK_FROM_GALLERY = 2;
    private static final String TEMP_PHOTO_FILE = "temporary_holder.jpg";
    private String user_name;
    private TextView txtpass_info,title;
    private final int SELECT_IMAGE=111;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        sp = getSharedPreferences("ObjectiveExam", 0);
        editor = sp.edit();
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView)mToolbar.findViewById(R.id.toolbar_title);
        title.setText("Sign Up");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        imageView1 = (ImageView) findViewById(R.id.imageView);
        isNaEditText = (EditText)findViewById(R.id.name);
        reg = (Button) findViewById(R.id.regi_in_button);
        txtpass_info = (TextView) findViewById(R.id.pass_info);
        isEmEditText = (EditText) findViewById(R.id.email);
        isPEditText = (EditText) findViewById(R.id.password);
        isCnEdtiText = (EditText) findViewById(R.id.con_password);
        school = (Spinner) findViewById(R.id.school);
        list_data = new ArrayList<HashMap<String, String>>();
        data = new HashMap<String, String>();
        reg.setOnClickListener(this);
        cd = new ConnectionDetector(getApplicationContext());
        if (cd.isConnectingToInternet()) {
            getSchool();
        } else {
            cd.showAlertDialog(RegisterActivity.this, "No Internet Connection",
                    "You don't have internet connection.", false);
        }
        imageView1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivityForResult(MediaStoreUtils.getPickImageIntent(RegisterActivity.this), PICK_FROM_GALLERY );
            }
        });

        isPEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // txtpass_info.setVisibility(View.VISIBLE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    txtpass_info.setVisibility(View.GONE);
                } else {
                    txtpass_info.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        File croppedImageFile = new File(RegisterActivity.this.getFilesDir(), "test.jpg");

        if ((requestCode == PICK_FROM_GALLERY) && (resultCode ==RESULT_OK)) {
            // When the user is done picking a picture, let's start the CropImage Activity,
            // setting the output image file and size to 200x200 pixels square.
            Uri croppedImage = Uri.fromFile(croppedImageFile);

            CropImageIntentBuilder cropImage = new CropImageIntentBuilder(200, 200, croppedImage);
            //cropImage.setOutlineColor(0xFF03A9F4);
            cropImage.setSourceImage(data.getData());
            editor.putBoolean("crop",true).apply();
            editor.commit();

            startActivityForResult(cropImage.getIntent(RegisterActivity.this), REQUEST_CROP_PICTURE);
        } else if (requestCode == REQUEST_CROP_PICTURE){
            if(resultCode == RESULT_OK){
                // When we are done cropping, display it in the ImageView.
                imageView1.setImageDrawable(null);
                Log.e("Path===============", "" + croppedImageFile.getAbsolutePath());
                editor.remove("crop").apply();
                editor.apply();
                Bitmap selectedImage = BitmapFactory.decodeFile(croppedImageFile.getAbsolutePath());
                imageView1.setImageBitmap(selectedImage);
                encodedImage = encodeTobase64(selectedImage);
            }
            editor.remove("crop").apply();
            editor.apply();
        }
    }

    private Uri getTempUri() {
        return Uri.fromFile(getTempFile());
    }

    private File getTempFile() {
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File file = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE);
            try {
                file.createNewFile();
            } catch (IOException e) {
            }
            return file;
        } else {

            return null;
        }
    }

    private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {
        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o);
        // The new size we want to scale to
        final int REQUIRED_SIZE = 140;
        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE
                    || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }
        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o2);
    }

    public static String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        Log.e("LOOK", imageEncoded);
        return imageEncoded;
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }
    // Email Validation
    private boolean isValidEmail(String email) {
        if(Character.isDigit(email.charAt(0))){
            return false;
        }else if(email.startsWith(".")){
            return false;
        }
        else if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return true;
        } else {
            return false;
        }
    }
    public Boolean Is_Valid_Person_Name(EditText edt)
            throws NumberFormatException {
        if (edt.getText().toString().trim().length() <= 0) {
            alertDialog("Please enter Name");
            user_name = null;
        } else if (edt.getText().toString().trim().length() <= 1) {
            alertDialog("Please enter More than one Alphabet");
            user_name = null;
        } else if (!edt.getText().toString().trim().matches("[a-zA-Z ]+")) {
            // edt.setError("Accept Alphabets Only.");
            alertDialog("Name Accept Alphabets Only.");
            user_name = null;
        } else {
            user_name = edt.getText().toString().trim();
            return true;
        }
        return false;
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 3;
    }

    private boolean isConPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.equals(password);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent i=new Intent(RegisterActivity.this,LoginActivity.class);
        startActivity(i);
        finish();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.regi_in_button:
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                Log.e("REg Button click", "REg Button click");
                name = isNaEditText.getText().toString().trim();
                email = isEmEditText.getText().toString().trim();
                password = isPEditText.getText().toString();
                con_password = isCnEdtiText.getText().toString().trim();
                String Text = school.getSelectedItem().toString().trim();
                int nameId = school.getSelectedItemPosition();
                if (!Is_Valid_Person_Name(isNaEditText)) {
                    //  alertDialog("Please enter Name");
                } else if (email.length() <= 0) {
                    alertDialog("Please enter Email id");
                } else if (!isValidEmail((email))) {
                    alertDialog("Please enter valid Email id");
                } else if (password.length() <= 0) {
                    alertDialog("Please enter Password");
                } else if (!isPasswordValid(password)) {
                    alertDialog("Password is too short");
                } else if (!password.equals(con_password)) {
                    alertDialog("Password do not match");
                } else if (nameId == 0) {
                    // Notify the selected item text
                    alertDialog("Please select school");
                } else {
                    schoolId = list_data.get(nameId - 1).get("school_id");
                    userRegister(email, password, schoolId, name);
                }
                break;
            default:
                break;
        }
    }

    private void getSchool() {
        final ProgressDialog dialog = new ProgressDialog(RegisterActivity.this,R.style.MyThemeHoloDialog);
        //dialog.setTitle("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(RegisterActivity.this);
        String requestURL = getResources().getString(R.string.web_service_url)+"getAll_school.php";
        Log.e("requestURL",requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(",", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                if (s != null) {
                    if (!s.startsWith("null")) {
                        try {
                            JSONObject jObj = new JSONObject(s);
                            if (jObj.getString("result").equals("success")) {
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                                if (jObj.getJSONArray("data").length() > 0) {
                                    schoolname_Array.add("Please select school");
                                    for (int a = 0; a < jObj.getJSONArray("data")
                                            .length(); a++) {
                                        JSONObject b = jObj.getJSONArray("data")
                                                .getJSONObject(a);
                                        data = new HashMap<String, String>();
                                        data.put("school_id", b.getString("school_id"));
                                        data.put("school_name",
                                                b.getString("school_name"));
                                        schoolname_Array.add(b.getString("school_name"));
                                        list_data.add(data);
                                    }
                                    // Initializing an ArrayAdapter
                                    adapter = new ArrayAdapter<String>(RegisterActivity.this,
                                            android.R.layout.simple_spinner_item, schoolname_Array){
                                        @Override
                                        public boolean isEnabled(int position){
                                            if(position == 0)
                                            {
                                                // Disable the first item from Spinner
                                                // First item will be use for hint
                                                return false;
                                            }
                                            else
                                            {
                                                return true;
                                            }
                                        }
                                        @Override
                                        public View getDropDownView(int position, View convertView,
                                                                    ViewGroup parent) {
                                            View view = super.getDropDownView(position, convertView, parent);
                                            TextView tv = (TextView) view;
                                            if(position == 0){
                                                // Set the hint text color gray
                                                tv.setTextColor(Color.GRAY);
                                            }
                                            else {
                                                tv.setTextColor(Color.BLACK);
                                            }
                                            return view;
                                        }
                                    };
                                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    school.setAdapter(adapter);
                                }
                            } else if (jObj.getString("result")
                                    .equals("failed")) {
                                alertDialog("No School List"
                                );
                                dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    } else {
                        dialog.dismiss();
                    }
                } else {
                    dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("VolleyError",""+volleyError);
            }
        });
        queue.add(stringRequest);
    }

    private void userRegister(final String user_email,final String user_pass,final String school_id,final String user_name) {
        final ProgressDialog dialog = new ProgressDialog(RegisterActivity.this,R.style.MyThemeHoloDialog);
        //dialog.setTitle("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(RegisterActivity.this);
        String requestURL = getResources().getString(R.string.web_service_url)+"User_registertion.php";
        if (encodedImage.equals("")) {
            Bitmap bm = BitmapFactory.decodeResource(getResources(),R.drawable.ic_profile);
            encodedImage = encodeTobase64(bm);
           // return;
        }
        JSONObject obj = new JSONObject();
        try {
            obj.put("img", "" + encodedImage);
            obj.put("user_email", user_email);
            try {
                obj.put("user_pass", MCrypt
                        .bytesToHex(mc.encrypt(user_pass)));
            } catch (Exception e) {
                e.printStackTrace();
            }
            obj.put("school_id", school_id);
            obj.put("name", user_name);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("JSONObject ", "" + obj.toString());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(",", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                if (s != null) {
                    if (!s.startsWith("null")) {
                        try {
                            JSONObject jObj = new JSONObject(s);
                            if (jObj.length() > 0) {
                                if (jObj.getString("result").equals("success")) {
                                    Log.d("onResponse ", "" + jObj.toString());
                                    editor.putString("students_id",
                                            jObj.getString("students_id"));
                                    editor.putString("student_name",user_name);
                                    editor.putString("students_email",
                                            jObj.getString("students_email"));
                                    editor.putString("school_id",
                                            jObj.getString("school_id"));
                                    editor.putString("school_name",
                                            jObj.getString("school_name"));
                                    editor.putString("student_image", jObj.getString("img"));
                                    editor.commit();
                                    //alertDialog("Register Success");
                                    //call Select Subject  Activity
                                    Intent in = new Intent(RegisterActivity.this, SelectSubjectActivity.class);
                                    startActivity(in);
                                    finish();
                                    if (dialog.isShowing()) {
                                        dialog.dismiss();
                                    }
                                } else {
                                    dialog.dismiss();
                                    alertDialog(jObj.getString("result"));
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    } else {
                        dialog.dismiss();
                    }
                } else {
                    dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("VolleyError",""+volleyError);
            }
        })
            {
                @Override
                protected Map<String, String> getParams () {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("user_email", user_email);
                    try {
                        params.put("user_pass", MCrypt
                                .bytesToHex(mc.encrypt(user_pass)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                params.put("school_id", school_id);
                params.put("name", user_name);
                params.put("img", "" + encodedImage);
                return params;
            }
                @Override
                public Map<String, String> getHeaders ()throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            };
            queue.add(stringRequest);
    }

    private void alertDialog(String errer) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                RegisterActivity.this);
        alertDialog.setTitle("OnlineExam");
        alertDialog.setMessage(errer);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                    }
                });
        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                RegisterActivity.this.finish();
            }
        });
        alertDialog.show();
    }
}
