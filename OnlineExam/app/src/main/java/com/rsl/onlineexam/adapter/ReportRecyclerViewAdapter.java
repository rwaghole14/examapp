package com.rsl.onlineexam.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.rsl.onlineexam.R;
import com.rsl.onlineexam.activity.ReportPageActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by admin on 11/23/2015.
 */
public class ReportRecyclerViewAdapter extends RecyclerView.Adapter<ReportRecyclerViewAdapter.ItemHolder> {
    private LayoutInflater layoutInflater;
    private Context context;

    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    public ArrayList<HashMap<String, String>> practiceQusetionsList;
    public static List<String> bookmarkList = new ArrayList<String>();
    public ReportRecyclerViewAdapter(Context context, ArrayList<HashMap<String, String>> practiceQusetionsList) {
        sp = context.getSharedPreferences("ObjectiveExam", 0);
        editor = sp.edit();
        this.context = context;
        this.practiceQusetionsList = practiceQusetionsList;
        layoutInflater = LayoutInflater.from(context);
        bookmarkList.clear();
        String id=sp.getString("students_id", "");
    }

    @Override
    public ItemHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        CardView itemCardView = (CardView) layoutInflater.inflate(
                R.layout.reportpage_cardview, viewGroup, false);
        return new ItemHolder(itemCardView, this);
    }

    @Override
    public void onBindViewHolder(final ItemHolder holder, final int position) {
            Log.e("ada", "Practics");

            holder.quetion_name.setText("Q"+ (ReportPageActivity.Practice_display_data.get(position).get("questionNo")) + ": " + ReportPageActivity.Practice_display_data.get(position).get("question"));
            holder.answer_name.setText(ReportPageActivity.Practice_display_data.get(position).get("Ans_option"));
            holder.explanation.setText("Explanation" + ": " + ReportPageActivity.Practice_display_data.get(position).get("Explanation"));
            holder.btn_option.setText(ReportPageActivity.Practice_display_data.get(position).get("Check"));
            if (ReportPageActivity.Practice_display_data.get(position).get("CorrectAns").equals(ReportPageActivity.Practice_display_data.get(position).get("option_id"))) {
                holder.btn_option.setBackgroundResource(R.color.greenclr);
            } else {
                holder.btn_option.setBackgroundResource(R.color.red);
            }
            if (bookmarkList.contains(ReportPageActivity.Practice_display_data.get(position).get("question_id"))) {
                holder.img_star.setImageResource(R.drawable.ic_star_yellow);
            } else {
                holder.img_star.setImageResource(R.drawable.ic_star_black);
            }
        holder.img_star.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bookmarkList.contains(ReportPageActivity.Practice_display_data.get(position).get("question_id"))) {
                    bookmarkList.remove(ReportPageActivity.Practice_display_data.get(position).get("question_id"));
                    holder.img_star.setImageResource(R.drawable.ic_star_black);
                } else {
                    bookmarkList.add(ReportPageActivity.Practice_display_data.get(position).get("question_id"));
                    holder.img_star.setImageResource(R.drawable.ic_star_yellow);
                }
                Log.e("bookmarkList", "" + bookmarkList);
            }
        });
    }

    @Override
    public int getItemCount() {

        return practiceQusetionsList.size();
    }

    public void add(int location, String iName) {

        notifyItemInserted(location);
    }

    public void remove(int location) {
        //  if (location >= topicList.size())
        //    return;
        //  notifyItemRemoved(location);
    }

    public static class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ReportRecyclerViewAdapter parent;
        private CardView cardView;
        private ItemClickListener clickListener;
        TextView quetion_name, answer_name, explanation;
        Button btn_option;
        ImageView img_star;
        boolean  clicked = false;
        public ItemHolder(CardView cView, ReportRecyclerViewAdapter parent) {
            super(cView);
            cardView = cView;
            this.parent = parent;
            quetion_name = (TextView) cardView.findViewById(R.id.question_done);
            answer_name = (TextView) cardView.findViewById(R.id.selected_ans);
            explanation = (TextView) cardView.findViewById(R.id.explanation);
            btn_option = (Button) cardView.findViewById(R.id.selected_option);
            img_star = (ImageView) cardView.findViewById(R.id.img_star);
            cView.setOnClickListener(this);
        }

        public void setClickListener(ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View v) {

        }
    }// class ItemHolder

    public interface ItemClickListener {
        void onClick(View view, int position);
    }
}
