package com.rsl.onlineexam.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rsl.onlineexam.R;
import com.rsl.onlineexam.activity.TestPageActivity;
import com.rsl.onlineexam.model.TestItem;

import java.util.List;

/**
 * Created by admin on 11/19/2015.
 */
public class TestRecyclerViewAdapter extends RecyclerView.Adapter<TestRecyclerViewAdapter.ItemHolder>
{
    private LayoutInflater layoutInflater;
    private Context context;
    TestItem item;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    public List<TestItem> testList;
    public SparseBooleanArray selectedItems;

    public TestRecyclerViewAdapter(Context context, List<TestItem> testList) {
        sp = context.getSharedPreferences("ObjectiveExam", 0);
        editor = sp.edit();
        this.context = context;
        this.testList = testList;
        layoutInflater = LayoutInflater.from(context);
    }
    @Override
    public ItemHolder onCreateViewHolder(
            ViewGroup viewGroup, int i) {
        CardView itemCardView = (CardView) layoutInflater.inflate(
                R.layout.test_cardview, viewGroup, false);
        return new ItemHolder(itemCardView, this);
    }

    @Override
    public void onBindViewHolder(ItemHolder itemHolder,
                                 final int i) {
        if(testList.get(i).getDone().equals("true")){
            itemHolder.Image_check.setVisibility(View.VISIBLE);

        }else{
            itemHolder.Image_check.setVisibility(View.INVISIBLE);
        }

        itemHolder.Image_display.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("onClick","onClickimg");
                editor.putString("test_id", testList.get(i).getTestId());
                editor.putString("test_name", testList.get(i).getTestName());
                editor.commit();
                Intent in = new Intent(context, TestPageActivity.class);
                in.putExtra("test_name", testList.get(i).getTestName());
                in.putExtra("done", testList.get(i).getDone());
                context.startActivity(in);
            }
        });
        itemHolder.testname.setText(testList.get(i).getTestName());

                itemHolder.linearly.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e("onClick","onClicklinearly");
                        editor.putString("test_id", testList.get(i).getTestId());
                        editor.putString("test_name", testList.get(i).getTestName());
                        editor.commit();
                        Intent in = new Intent(context, TestPageActivity.class);
                        in.putExtra("test_name", testList.get(i).getTestName());
                        in.putExtra("done", testList.get(i).getDone());
                        context.startActivity(in);
                    }
                });

        itemHolder.setClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                Log.e("onClick","onClickitemHolder");
                editor.putString("test_id", testList.get(i).getTestId());
                editor.putString("test_name", testList.get(i).getTestName());
                editor.commit();
                Intent in = new Intent(context, TestPageActivity.class);
                in.putExtra("test_name", testList.get(i).getTestName());
                in.putExtra("done", testList.get(i).getDone());
                context.startActivity(in);
            }
        });
    }

    @Override
    public int getItemCount() {
        return testList.size();
    }
    public static class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TestRecyclerViewAdapter parent;
        private CardView cardView;
        private ItemClickListener clickListener;
        TextView testname;
        LinearLayout linearly;
        ImageView Image_check;
        ImageView Image_display;

        public ItemHolder(CardView cView, TestRecyclerViewAdapter parent) {
            super(cView);
            cardView = cView;
            this.parent = parent;
            linearly = (LinearLayout) cardView.findViewById(R.id.linearly);
            testname = (TextView) cardView.findViewById(R.id.test_name);
            Image_check = (ImageView)cardView.findViewById(R.id.image_check);
            Image_display = (ImageView)cardView.findViewById(R.id.toolbar_image);
            cView.setOnClickListener(this);
        }

        public void setClickListener(ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View v) {
          clickListener.onClick(v, getPosition());

        }
    }// class ItemHolder

    public interface ItemClickListener {
        void onClick(View view, int position);
    }
}
