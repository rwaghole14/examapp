package com.rsl.onlineexam.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.rsl.onlineexam.R;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by admin on 11/21/2015.
 */
public class ReviewRecyclerViewAdapter extends RecyclerView.Adapter<ReviewRecyclerViewAdapter.ItemHolder>  {
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<HashMap<String,String>> testList =new  ArrayList<HashMap<String,String>>();
    private SharedPreferences sp;
    private ArrayList<HashMap<String,String>> testFixList =new  ArrayList<HashMap<String,String>>();
    private SharedPreferences.Editor editor;

    public ReviewRecyclerViewAdapter(Context context,  ArrayList<HashMap<String,String>> testList) {
        sp = context.getSharedPreferences("ObjectiveExam", 0);
        editor = sp.edit();
        this.context = context;
        this.testList.addAll(testList);
        this.testFixList.addAll(testList);
        layoutInflater = LayoutInflater.from(context);
    }

    public void filterData(ArrayList<String> filter){
        Log.e("filterData","filter size========"+filter.size());


        testList.clear();
        if(filter.size() > 0) {
            Log.e("filterData","testFixList size========"+testFixList.size());
            for (int i = 0; i < testFixList.size(); i++) {
                HashMap<String, String> s = testFixList.get(i);
                String tn = s.get("topic_name");
                String bf = s.get("bookmark_flag");
                String af = s.get("answer_flag");
                Log.e("topic_name", tn);
                boolean wrong = false, bookmarked = false, topic = false;

                if (filter.contains("Wrong")) {
                    if (af.equals("false"))
                        wrong = true;
                } else {
                    wrong = true;
                }
                if (filter.contains("Bookmarked")) {
                    if (bf.equals("true")) {
                        bookmarked = true;
                    }
                } else
                    bookmarked = true;
                if (filter.contains(tn)) {
                    //testList.add(s);
                    topic = true;
                } else {
                    if ((filter.size() == 1 && (filter.contains("Bookmarked") || filter.contains("Wrong"))) || (filter.size() == 2 && (filter.contains("Bookmarked") && filter.contains("Wrong"))))
                        topic = true;
                }
                if (topic && bookmarked && wrong)
                    testList.add(s);
            }
        }else{

            testList.addAll(testFixList);
        }
        if(testList.size()==0)
            Toast.makeText(context,"No result found!!",Toast.LENGTH_SHORT).show();
            Log.e("filterData","testList size========"+testList.size());
            notifyDataSetChanged();
    }

    @Override
    public ItemHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        CardView itemCardView = (CardView) layoutInflater.inflate(
                R.layout.reviewcardview, viewGroup, false);
        return new ItemHolder(itemCardView, this);
    }

    @Override
    public void onBindViewHolder(ItemHolder holder, int position) {
        HashMap<String,String> temp = testList.get(position);
            holder.quetion_name.setText("Q"+(position+1)+": "+temp.get("question"));
            holder.answer_name.setText(temp.get("answer"));
        if (temp.get("answer_flag").equals("false")) {
            holder.answer_option.setTextColor(Color.RED);
        } else {
            holder.answer_option.setTextColor(Color.parseColor("#22b573"));
        }
    }
    @Override
    public int getItemCount() {
        return testList.size();
    }
    public void add(int location, String iName) {

        notifyItemInserted(location);
    }

    public void remove(int location) {
      //  if (location >= topicList.size())
        //    return;
      //  notifyItemRemoved(location);
    }
    public static class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ReviewRecyclerViewAdapter parent;
        private CardView cardView;
        private ItemClickListener clickListener;
        TextView quetion_name,answer_name,answer_option;
        public ItemHolder(CardView cView, ReviewRecyclerViewAdapter parent) {
            super(cView);
            cardView = cView;
            this.parent = parent;
            quetion_name = (TextView) cardView.findViewById(R.id.question_name);
            answer_name = (TextView) cardView.findViewById(R.id.answer_name);
            answer_option = (TextView) cardView.findViewById(R.id.answer_option);
            cView.setOnClickListener(this);
        }

        public void setClickListener(ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View v) {
            //clickListener.onClick(v, getPosition());
        }
    }// class ItemHolder
    public interface ItemClickListener {
        void onClick(View view, int position);
    }
}
