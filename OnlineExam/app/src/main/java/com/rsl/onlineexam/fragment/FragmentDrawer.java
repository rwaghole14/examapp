package com.rsl.onlineexam.fragment;

/**
 * Created by Ravi on 29/07/15.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.camera.CropImageIntentBuilder;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.onlineexam.R;
import com.rsl.onlineexam.activity.MediaStoreUtils;
import com.rsl.onlineexam.adapter.NavigationDrawerAdapter;
import com.rsl.onlineexam.model.NavDrawerItem;
import com.rsl.onlineexam.model.RoundImage;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FragmentDrawer extends Fragment {

    private static String TAG = FragmentDrawer.class.getSimpleName();
    private RecyclerView recyclerView;
    private SharedPreferences sp;
    SharedPreferences.Editor editor;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private NavigationDrawerAdapter adapter;
    private View containerView;
    private TextView text_user,school_name;
    ImageView user_image;
    private static final int PICK_FROM_GALLERY = 2;
    Bitmap bitmap;
    RoundImage roundedImage;
    RelativeLayout rl;
    private static String[] titles = null;
    private static int icon[] = { R.drawable.ic_home,
            R.drawable.ic_phy,
            R.drawable.ic_chem};
    private FragmentDrawerListener drawerListener;
    private static final int REQUEST_CROP_PICTURE = 786;
    String encodedImage = "";
    private static final String TEMP_PHOTO_FILE = "temporary_holder.jpg";
    public FragmentDrawer() {
    }

    public void setDrawerListener(FragmentDrawerListener listener) {
        this.drawerListener = listener;
    }

    public static List<NavDrawerItem> getData() {
        List<NavDrawerItem> data = new ArrayList<>();
        // preparing navigation drawer items
        for (int i = 0; i < titles.length; i++) {
            NavDrawerItem navItem = new NavDrawerItem();
            navItem.setTitle(titles[i]);
            navItem.setIcon(icon[i]);
            data.add(navItem);
        }
        return data;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // drawer labels
        titles = getActivity().getResources().getStringArray(R.array.nav_drawer_labels);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflating view layout
        View layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        sp = getActivity().getSharedPreferences("ObjectiveExam", 0);
        editor = sp.edit();
        user_image = (ImageView) layout.findViewById(R.id.imageView);
        text_user = (TextView) layout.findViewById(R.id.txt_user_name);
        school_name = (TextView)layout.findViewById(R.id.txt_school_name);
        recyclerView = (RecyclerView) layout.findViewById(R.id.drawerList);
        rl = (RelativeLayout)layout.findViewById(R.id.nav_header_container);
        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("RelativeLayout","");
            }
        });
        adapter = new NavigationDrawerAdapter(getActivity(), getData());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                drawerListener.onDrawerItemSelected(view, position);
                mDrawerLayout.closeDrawer(containerView);
            }
            @Override
            public void onLongClick(View view, int position) {
            }
        }));
        user_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(MediaStoreUtils.getPickImageIntent(getActivity()), PICK_FROM_GALLERY );
            }
        });
        if(sp.getString("student_name","").equals("")||sp.getString("school_name","").equals("")){
            Log.e("Student name if", sp.getString("student_name","")+"+"+sp.getString("school_name",""));
            text_user.setText("Stusent Name");
            school_name.setText("School Name");

        }else {
            Log.e("Student name else", sp.getString("student_name", "") + "+" + sp.getString("school_name", ""));
            //text_user.setText(sp.getString("student_name",""));
            //school_name.setText(sp.getString("school_name",""));

            text_user.setText(sp.getString("student_name",""));
            school_name.setText(sp.getString("school_name",""));
        }
        if(sp.getString("student_image","").equals("")){
            Log.e("Student image if",sp.getString("student_image",""));
            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_profile);
            roundedImage = new RoundImage(bm);
            user_image.setImageBitmap(bm);
        }else {
            String pimg = getResources().getString(R.string.web_service_url)
                    + sp.getString("student_image","");
            Log.e("Student image else", pimg);
            //Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_profile);
            if(!sp.getBoolean("crop",false)) {
                new LoadImage().execute(pimg);
            }
        }
        return layout;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        File croppedImageFile = new File(getActivity().getFilesDir(), "test.jpg");
        if ((requestCode == PICK_FROM_GALLERY) && (resultCode == getActivity().RESULT_OK)) {
            // When the user is done picking a picture, let's start the CropImage Activity,
            // setting the output image file and size to 200x200 pixels square.
            Uri croppedImage = Uri.fromFile(croppedImageFile);
            CropImageIntentBuilder cropImage = new CropImageIntentBuilder(200, 200, croppedImage);
            //cropImage.setOutlineColor(0xFF03A9F4);
            cropImage.setSourceImage(data.getData());
            editor.putBoolean("crop",true).apply();
            editor.commit();
            startActivityForResult(cropImage.getIntent(getActivity()), REQUEST_CROP_PICTURE);
        } else if (requestCode == REQUEST_CROP_PICTURE){
            if(resultCode == getActivity().RESULT_OK){
            // When we are done cropping, display it in the ImageView.
            user_image.setImageDrawable(null);
            Log.e("Path===============", "" + croppedImageFile.getAbsolutePath());
                editor.remove("crop").apply();
                editor.apply();
                Bitmap selectedImage = BitmapFactory.decodeFile(croppedImageFile.getAbsolutePath());
                user_image.setImageBitmap(selectedImage);
                encodedImage = encodeTobase64(selectedImage);
                update_image();
        }
            editor.remove("crop").apply();
            editor.apply();
    }
    }

    private Uri getTempUri() {
        return Uri.fromFile(getTempFile());
    }

    private File getTempFile() {

        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File file = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE);
            try {
                file.createNewFile();
            } catch (IOException e) {
            }
            return file;
        } else {

            return null;
        }
    }

    private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {

        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(selectedImage), null, o);
        // The new size we want to scale to
        final int REQUIRED_SIZE = 140;
        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE
                    || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }
        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(selectedImage), null, o2);
    }

    public static String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        Log.e("LOOK", imageEncoded);
        return imageEncoded;
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }
    private class LoadImage extends AsyncTask<String, String, Bitmap> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream)new URL(args[0]).getContent());

            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {

            if(image != null){
                roundedImage = new RoundImage(image);
                user_image.setImageDrawable(roundedImage);
            }else{
                Toast.makeText(getActivity(), "Image Does Not exist or Network Error", Toast.LENGTH_SHORT).show();
            }
        }
    }
//    public static Bitmap getBitmapFromURL(String src) {
//        try {
//            URL url = new URL(src);
//            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//            connection.setDoInput(true);
//            connection.connect();
//            InputStream input = connection.getInputStream();
//            Bitmap myBitmap = BitmapFactory.decodeStream(input);
//            return myBitmap;
//        } catch (IOException e) {
//            // Log exception
//            return null;
//        }
//    }
    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                toolbar.setAlpha(1 - slideOffset / 2);
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }

    public static interface ClickListener {
        public void onClick(View view, int position);
        public void onLongClick(View view, int position);
    }

    static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;
        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }
                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
    public interface FragmentDrawerListener {
        public void onDrawerItemSelected(View view, int position);
    }
    private void update_image() {
        final ProgressDialog dialog = new ProgressDialog(getActivity(),R.style.MyThemeHoloDialog);
        //dialog.setTitle("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(getActivity());
        String requestURL =getResources().getString(R.string.web_service_url)+"update_image.php";
        if (encodedImage.equals("")) {
            Bitmap bm = BitmapFactory.decodeResource(getResources(),R.drawable.ic_profile);
            encodedImage = encodeTobase64(bm);
            // return;
        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(",", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                if (s != null) {
                    if (!s.startsWith("null")) {
                        try {
                            JSONObject jObj = new JSONObject(s);
                            if (jObj.length() > 0) {
                                if (jObj.getString("result").equals("success")) {
                                    Log.d("onResponse ", "" + jObj.toString());
                                    editor.putString("student_image", jObj.getString("img"));
                                    editor.commit();
                                    if (dialog.isShowing()) {
                                        dialog.dismiss();
                                    }
                                } else {
                                    dialog.dismiss();
                                    alertDialog(jObj.getString("result"));
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    } else {
                        dialog.dismiss();
                    }
                } else {
                    dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("VolleyError",""+volleyError);
            }
        })
        {
            @Override
            protected Map<String, String> getParams () {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("user_id",sp.getString("students_id",""));
                params.put("img", "" + encodedImage);
                return params;
            }
            @Override
            public Map<String, String> getHeaders ()throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(stringRequest);
    }
    private void alertDialog(String errer) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
               getActivity());
        alertDialog.setTitle("OnlineExam");
        alertDialog.setMessage(errer);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                    }
                });
        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                getActivity().finish();
            }
        });
        alertDialog.show();
    }
}
