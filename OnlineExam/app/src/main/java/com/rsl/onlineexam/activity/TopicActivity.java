package com.rsl.onlineexam.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.onlineexam.R;
import com.rsl.onlineexam.adapter.TopicRecyclerViewAdapter;
import com.rsl.onlineexam.model.TopicItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 11/7/2015.
 */
public class TopicActivity extends AppCompatActivity {
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private Toolbar mToolbar;
    private TopicRecyclerViewAdapter myRecyclerViewAdapter;
    private RecyclerView recList;
    ArrayList<TopicItem> list_data;
    private ConnectionDetector cd;
    Button all_topic;
    boolean isSelected = false;
    private TextView mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic_selection);
        sp = getSharedPreferences("ObjectiveExam", 0);
        editor = sp.edit();

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("Topic");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recList = (RecyclerView) findViewById(R.id.cardList);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        list_data = new ArrayList<TopicItem>();
        all_topic = (Button) findViewById(R.id.all_topic_button);
        all_topic.setVisibility(View.GONE);
        cd = new ConnectionDetector(getApplicationContext());
        if (cd.isConnectingToInternet()) {
            getAlltopic(sp.getString("SubjectId", ""));
        } else {
            cd.showAlertDialog(TopicActivity.this, "No Internet Connection",
                    "You don't have internet connection.", false);
        }
    }
    private void getAlltopic(final String subId) {
        final ProgressDialog dialog = new ProgressDialog(TopicActivity.this,R.style.MyThemeHoloDialog);
        // dialog.setTitle("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(TopicActivity.this);
        String requestURL = getResources().getString(R.string.web_service_url)+"getAll_topic.php";
        JSONObject obj = new JSONObject();
        try {
            obj.put("sub_id", subId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("JSONObject ", "" + obj.toString());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(",", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                if (s != null) {
                    Log.e("JSONObject ",s);
                    if (!s.startsWith("null")) {
                        try {
                            JSONObject jObj = new JSONObject(s);
                            if (jObj.getString("result").equals("success")) {
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                                if (jObj.getJSONArray("data").length() > 0) {
                                    TopicItem data = new TopicItem();
                                    data.setTopicId("0");
                                    data.setTopicName("All");
                                    list_data.add(data);
                                    for (int a = 0; a < jObj.getJSONArray("data")
                                            .length(); a++) {
                                        JSONObject b = jObj.getJSONArray("data")
                                                .getJSONObject(a);
                                        TopicItem data1 = new TopicItem();
                                        data1.setTopicId(b.getString("topic_id"));
                                        data1.setTopicName(b.getString("topic_name"));
                                        list_data.add(data1);
                                    }
                                    myRecyclerViewAdapter = new TopicRecyclerViewAdapter(TopicActivity.this, list_data);
                                    recList.setAdapter(myRecyclerViewAdapter);
                                }
                            } else if (jObj.getString("result")
                                    .equals("failed")) {
                                // alertDialog(jObj.getString("No School List"));
                                dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    } else {
                        dialog.dismiss();
                    }
                } else {
                    dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
            }
        }){
            @Override
            protected Map<String, String> getParams () {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("sub_id", subId);
                Log.e("JSONObject ", "" + params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders ()throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if(MainActivity.topics.size()==0) {
            Toast.makeText(this, "Please select at least one topic!!", Toast.LENGTH_SHORT).show();
            return;
        }
        //super.onBackPressed();
        AlertDialog.Builder alert = new AlertDialog.Builder(TopicActivity.this);
        alert.setTitle("Confirm");
        alert.setIcon(R.mipmap.ic_launcher);
        alert.setMessage("Are you sure you want to go back?");
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                editor.putString("selectTopicsId", TextUtils.join(",", MainActivity.topics)).apply();
                editor.commit();
                finish();
            }
        });
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert.create().show();
    }
}
