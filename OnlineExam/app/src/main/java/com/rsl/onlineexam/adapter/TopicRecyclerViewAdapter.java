package com.rsl.onlineexam.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rsl.onlineexam.R;
import com.rsl.onlineexam.activity.MainActivity;
import com.rsl.onlineexam.model.TopicItem;

import java.util.List;

/**
 * Created by admin on 11/7/2015.
 */
public class TopicRecyclerViewAdapter extends RecyclerView.Adapter<TopicRecyclerViewAdapter.ItemHolder> {
    private LayoutInflater layoutInflater;
    private Context context;
    TopicItem item;
    int count=0;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    public static List<TopicItem> topicList;
    //public static List<String> selectedTopics=null;
    public SparseBooleanArray selectedItems;

    public TopicRecyclerViewAdapter(Context context, List<TopicItem> topicList) {
        sp = context.getSharedPreferences("ObjectiveExam", 0);
        editor = sp.edit();
       // selectedTopics = new ArrayList<String>();
        this.context = context;
        this.topicList = topicList;
        layoutInflater = LayoutInflater.from(context);
        if(MainActivity.topics.size()==0||MainActivity.topics.contains(""+0)) {
            MainActivity.topics.clear();
            for (int i = 0; i < topicList.size(); i++) {
                MainActivity.topics.add("" + i);
            }
        }
        //selectedTopics.clear();
    }

    @Override
    public ItemHolder onCreateViewHolder(
            ViewGroup viewGroup, int i) {
        CardView itemCardView = (CardView) layoutInflater.inflate(
                R.layout.topic_cardview, viewGroup, false);
        return new ItemHolder(itemCardView, this);
    }

    @Override
    public void onBindViewHolder(final ItemHolder itemHolder,
                                  final int i) {
        if(i==0){
            itemHolder.Subjectname.setText("All");
        }else {
            itemHolder.Subjectname.setText(topicList.get(i).getTopicName());
            //itemHolder.count.setText(""+videoFileName.length);
        }
        itemHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ( MainActivity.topics.contains("" + i)) {
                    //selectedTopics.remove("" + i);
                    Log.e("topics if",""+i);
                    MainActivity.topics.remove("" + i);
                    //selectedTopics.remove("0");
                   // MainActivity.topics.remove("0");
                    itemHolder.linearLayout.setBackgroundResource(R.drawable.button_gray);
                } else {
                    MainActivity.topics.add("" + i);
                    //selectedTopics.add("" + i);
                    itemHolder.linearLayout.setBackgroundResource(R.drawable.button_green);
                }
                if (i == 0) {
                    MainActivity.topics.clear();
                    for(int i = 0;i<topicList.size();i++) {
                        //selectedTopics.add(""+i);
                        MainActivity.topics.add(""+i);
                    }
                    notifyDataSetChanged();
                } else {
                    MainActivity.topics.remove("0");
                    notifyDataSetChanged();
                }
                if(MainActivity.topics.size()==topicList.size()-1){
                    MainActivity.topics.add("0");
                    notifyDataSetChanged();
                }
            }
        });
        Log.e("topics id",""+MainActivity.topics);
        if( MainActivity.topics.contains(""+i))
            itemHolder.linearLayout.setBackgroundResource(R.drawable.button_green);
        else
            itemHolder.linearLayout.setBackgroundResource(R.drawable.button_gray);
    }
    @Override
    public int getItemCount() {
        return topicList.size();
    }

    public void add(int location, String iName) {
        notifyItemInserted(location);
    }

    public void remove(int location) {
        if (location >= topicList.size())
            return;
        notifyItemRemoved(location);
    }

    public static class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TopicRecyclerViewAdapter parent;
        private CardView cardView;
        private ItemClickListener clickListener;
        TextView Subjectname;
        boolean isSelected = false;
        ImageView Image_display;
        LinearLayout linearLayout;

        public ItemHolder(CardView cView, TopicRecyclerViewAdapter parent) {
            super(cView);
            cardView = cView;
            this.parent = parent;
            Subjectname = (TextView) cardView.findViewById(R.id.subject_name);
            linearLayout = (LinearLayout) cardView.findViewById(R.id.ly);
            //cView.setOnClickListener(this);
        }

        public void setClickListener(ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View v) {

        }
    }// class ItemHolder

    public interface ItemClickListener {
        void onClick(View view, int position);
    }
}
