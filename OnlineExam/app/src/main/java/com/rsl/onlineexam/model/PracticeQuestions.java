package com.rsl.onlineexam.model;

/**
 * Created by admin on 11/21/2015.
 */
public class PracticeQuestions {
    String questions;
    String currectans;

    public String getQuestionNo1() {
        return questionNo1;
    }

    public void setQuestionNo1(String questionNo1) {
        this.questionNo1 = questionNo1;
    }

    String questionNo1;
    String questionNo2;

    public String getQuestionNo2() {
        return questionNo2;
    }

    public void setQuestionNo2(String questionNo2) {
        this.questionNo2 = questionNo2;
    }

    public String getQuestionNo3() {
        return questionNo3;
    }

    public void setQuestionNo3(String questionNo3) {
        this.questionNo3 = questionNo3;
    }

    public String getQuestionNo4() {
        return questionNo4;
    }

    public void setQuestionNo4(String questionNo4) {
        this.questionNo4 = questionNo4;
    }

    String questionNo3;
    String questionNo4;

    public String getSelectedOptionId() {
        return selectedOptionId;
    }

    public void setSelectedOptionId(String selectedOptionId) {
        this.selectedOptionId = selectedOptionId;
    }

    String selectedOptionId = "";

    public String getSelectedOptionAns() {
        return selectedOptionAns;
    }

    public void setSelectedOptionAns(String selectedOptionAns) {
        this.selectedOptionAns = selectedOptionAns;
    }

    String selectedOptionAns = "";

    public String getSelectedQuestionNo() {
        return selectedQuestionNo;
    }

    public void setSelectedQuestionNo(String selectedQuestionNo) {
        this.selectedQuestionNo = selectedQuestionNo;
    }

    String selectedQuestionNo = "";

    public String getQuestions() {
        return questions;
    }

    public void setQuestions(String questions) {
        this.questions = questions;
    }

    public String getCurrectans() {
        return currectans;
    }

    public void setCurrectans(String currectans) {
        this.currectans = currectans;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getOptionId1() {
        return optionId1;
    }

    public void setOptionId1(String optionId1) {
        this.optionId1 = optionId1;
    }

    public String getOption1() {
        return option1;
    }

    public void setOption1(String option1) {
        this.option1 = option1;
    }

    public String getOptionId2() {
        return optionId2;
    }

    public void setOptionId2(String optionId2) {
        this.optionId2 = optionId2;
    }

    public String getOption2() {
        return option2;
    }

    public void setOption2(String option2) {
        this.option2 = option2;
    }

    public String getOptionId3() {
        return optionId3;
    }

    public void setOptionId3(String optionId3) {
        this.optionId3 = optionId3;
    }

    public String getOption3() {
        return option3;
    }

    public void setOption3(String option3) {
        this.option3 = option3;
    }

    public String getOptionId4() {
        return optionId4;
    }

    public void setOptionId4(String optionId4) {
        this.optionId4 = optionId4;
    }

    public String getOption4() {
        return option4;
    }

    public void setOption4(String option4) {
        this.option4 = option4;
    }

    String questionId;
    String optionId1;
    String option1;
    String optionId2;
    String option2;
    String optionId3;
    String option3;
    String optionId4;
    String option4;
    String topicId;

    public String getSelectedOption() {
        return selectedOption;
    }

    public void setSelectedOption(String selectedOption) {
        this.selectedOption = selectedOption;
    }

    String selectedOption = "";

    public String getExplation() {
        return explation;
    }

    public void setExplation(String explation) {
        this.explation = explation;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    String explation;

    public PracticeQuestions() {

    }

    public PracticeQuestions(String questions, String currectans, String questionId,
                             String optionId1, String option1, String optionId2,
                             String option2, String optionId3, String option3, String questionNo1, String questionNo2, String questionNo3
            , String questionNo4
            , String optionId4, String option4, String topicId, String explation) {
        this.questions = questions;
        this.currectans = currectans;
        this.questionId = questionId;
        this.optionId1 = optionId1;
        this.option1 = option1;
        this.optionId2 = optionId2;
        this.option2 = option2;
        this.optionId3 = optionId3;
        this.option3 = option3;
        this.optionId4 = optionId4;
        this.option4 = option4;
        this.topicId = topicId;
        this.explation = explation;
        this.questionNo1 = questionNo1;
        this.questionNo2 = questionNo2;
        this.questionNo3 = questionNo3;
        this.questionNo4 = questionNo4;
    }
}
