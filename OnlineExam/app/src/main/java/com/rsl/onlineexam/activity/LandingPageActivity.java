package com.rsl.onlineexam.activity;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.rsl.onlineexam.R;


public class LandingPageActivity extends Activity {
	ImageView image;
	SharedPreferences sp;
	SharedPreferences.Editor editor;
	private ConnectionDetector cd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_landing_page);
		sp = getSharedPreferences("ObjectiveExam", 0);
		editor = sp.edit();
		cd = new ConnectionDetector(getApplicationContext());
		image = (ImageView) findViewById(R.id.imageView_landing);
		Animation animationrotate = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.zoom);
		image.startAnimation(animationrotate);
		editor.remove("selectTopicsId").apply();
		editor.commit();
		if (!cd.isConnectingToInternet()) {
			alertDialog("You don't have internet connection");
//   cd.showAlertDialog(LandingPageActivity.this, "No Internet Connection",
//     "You don't have internet connection.", false);
		} else {
			Thread background = new Thread() {
				public void run() {
					try {
						// Thread will sleep for 5 seconds
						sleep(4 * 1000);
						// After 5 seconds redirect to another intent
						if (sp.getString("students_id", "no user").equals("no user")) {
							Log.v("if Spash_Screen", "if Spash_Screen");
							Intent i = new Intent(LandingPageActivity.this,
									LoginActivity.class);
							startActivity(i);
							finish();
						} else {
							Log.v("else Spash_Screen", "else Spash_Screen");
							Intent i = new Intent(LandingPageActivity.this,
									MainActivity.class);
							startActivity(i);
							finish();
						}
						// Remove activity
						//finish();
					} catch (Exception e) {
					}
				}
			};
			// start thread
			background.start();
		}//else
	}
	private void alertDialog(String errer) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				LandingPageActivity.this);
		alertDialog.setTitle("OnlineExam");
		alertDialog.setMessage(errer);
		alertDialog.setCancelable(false);
		alertDialog.setIcon(R.mipmap.ic_launcher);
		alertDialog.setPositiveButton("Ok",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						finish();
					}
				});
		alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				LandingPageActivity.this.finish();
			}
		});
		alertDialog.show();
	}
}
