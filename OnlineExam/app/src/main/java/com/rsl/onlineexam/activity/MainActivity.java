package com.rsl.onlineexam.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.rsl.onlineexam.R;
import com.rsl.onlineexam.fragment.FragmentDrawer;
import com.rsl.onlineexam.fragment.HomeFragment;
import com.rsl.onlineexam.fragment.ReviewFragment;
import com.rsl.onlineexam.fragment.TestFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {

    private static String TAG = MainActivity.class.getSimpleName();
    public static Toolbar mToolbar;
    public static TextView mTitle;
    private FragmentDrawer drawerFragment;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    public  static boolean flag = false;
    public static List<String> topics = new  ArrayList<String>();;
    int pos=1;
    public static boolean fragmentFlag = false;
    public  static  MenuItem item;
    private ConnectionDetector cd;
    public static String currentF = "home";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sp = getSharedPreferences("ObjectiveExam", 0);
        editor = sp.edit();
        cd = new ConnectionDetector(getApplicationContext());
        if (!cd.isConnectingToInternet()) {
            cd.showAlertDialog(MainActivity.this, "No Internet Connection",
                    "You don't have internet connection.", false);
        }
        pos = getIntent().getIntExtra("pos",1);
        Log.e("Main Activity", getResources().getString(R.string.web_service_url));
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(sp.getString("SubjectName", ""));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);
        // display the first navigation drawer view on app launch
        displayView(Integer.parseInt(sp.getString("SubjectId","1")),true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTitle.setText(sp.getString("SubjectName", ""));
        if(flag){
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            HomeFragment fragment = new HomeFragment();
            Bundle args = new Bundle();
            args.putString("CID", "Test");
            fragment.setArguments(args);
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
            flag=false;
        }
        if(sp.getString("selectTopicsId","").equals("")){
            Log.e("MainActivity if",sp.getString("selectTopicsId",""));
            topics.add("0");
        }else {
            Log.e("MainActivity else", sp.getString("selectTopicsId", ""));
            String serialized = sp.getString("selectTopicsId", null);
            topics = new ArrayList<>(Arrays.asList(TextUtils.split(serialized, ",")));
            Log.e("Main else",""+topics);
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
        alert.setTitle("Confirm");
        alert.setIcon(R.mipmap.ic_launcher);
        alert.setMessage("Are you sure you want to Exit?");
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert.create().show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        item = menu.findItem(R.id.action_settings);
        if(!currentF.equals("home"))
            item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent topic = new Intent(MainActivity.this,TopicActivity.class);
            startActivity(topic);
            //finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position, false);
    }
    private void displayView(int position,boolean flag) {
       Fragment fragment = null;
         String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                Intent in = new Intent(MainActivity.this,SelectSubjectActivity.class);
                startActivity(in);
                finish();
                break;
            case 1:
               // title = sp.getString("SubjectName", "");
                title = "Physics";
                //mToolbar.setTitle("Physics");
                editor.putString("SubjectName", "Physics").apply();
                editor.putString("SubjectId", "" + 1).apply();
                editor.commit();
                if(!flag){
                    fragmentFlag=true;
                    if(currentF.equals("review")) {
                        ReviewFragment.MakeAPICall();
                    }else if(currentF.equals("test")) {
                        TestFragment.getAlltest();
                        Log.e("MainActivity",""+currentF);
                    }else if(currentF.equals("home")) {
                        mTitle.setText("Physics");
                    }
                    MainActivity.item.setVisible(false);
                }else{
                    mTitle.setText("Physics");
                    fragment = new HomeFragment();
                }
                topics=new  ArrayList<String>();
                break;
            case 2:
                //title = sp.getString("SubjectName", "");
                title = "Chemistry";
               // mToolbar.setTitle("Chemistry");
                editor.putString("SubjectName", "Chemistry").apply();
                editor.putString("SubjectId", "" + 2).apply();
                editor.commit();
                if(!flag) {
                    fragmentFlag = true;
                    if(currentF.equals("review"))
                        ReviewFragment.MakeAPICall();
                    else if(currentF.equals("test"))
                        TestFragment.getAlltest();
                    else if(currentF.equals("home"))
                        mTitle.setText("Chemistry");
                    MainActivity.item.setVisible(false);
                }else{
                    mTitle.setText("Chemistry");
                    fragment = new HomeFragment();
                }
                topics=new  ArrayList<String>();
                break;
            default:
                break;
        }
        if (fragment != null && flag) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            Bundle args = new Bundle();
            args.putString("CID", "");
            fragment.setArguments(args);
            fragmentTransaction.commit();
            // set the toolbar title
           // getSupportActionBar().setTitle(title);
        }
    }
}