package com.rsl.onlineexam.adapter;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rsl.onlineexam.R;
import com.rsl.onlineexam.activity.MainActivity;
import com.rsl.onlineexam.model.SubjectItem;

import java.util.List;


public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ItemHolder> {


    private LayoutInflater layoutInflater;
    private Activity context;
    SubjectItem item;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    public static List<SubjectItem> subjectList;

    public MyRecyclerViewAdapter(Activity context, List<SubjectItem> subjectList) {
        sp = context.getSharedPreferences("ObjectiveExam", 0);
        editor = sp.edit();
        this.context = context;
        this.subjectList = subjectList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public ItemHolder onCreateViewHolder(
            ViewGroup viewGroup, int i) {

        CardView itemCardView = (CardView) layoutInflater.inflate(
                R.layout.subject_cardview, viewGroup, false);
        return new ItemHolder(itemCardView, this);
    }

    @Override
    public void onBindViewHolder(ItemHolder itemHolder,
                                 final int i) {
        itemHolder.Subjectname.setText(subjectList.get(i).getSubjectName());
        //itemHolder.count.setText(""+videoFileName.length);
        itemHolder.setClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                Log.e("school_name", subjectList.get(position).getSubjectName());
                editor.putString("SubjectName", subjectList.get(position).getSubjectName());
                editor.putString("SubjectId", subjectList.get(position).getSubjectId());
                editor.commit();
                Intent in=new Intent(context, MainActivity.class);
                in.putExtra("pos",i+1);
                in.putExtra("SubjectName",subjectList.get(position).getSubjectName());
                in.putExtra("SubjectId", subjectList.get(position).getSubjectId());
                context.startActivity(in);
                context.finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return subjectList.size();
    }

    public void add(int location, String iName) {

        notifyItemInserted(location);
    }

    public void remove(int location) {
        if (location >= subjectList.size())
            return;
        notifyItemRemoved(location);
    }

    public static class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private MyRecyclerViewAdapter parent;
        private CardView cardView;
        private ItemClickListener clickListener;
        TextView Subjectname;
        ImageView Image_display;

        public ItemHolder(CardView cView, MyRecyclerViewAdapter parent) {
            super(cView);
            cardView = cView;
            this.parent = parent;
            Subjectname = (TextView) cardView.findViewById(R.id.subject_name);
            cView.setOnClickListener(this);
        }

        public void setClickListener(ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View v) {
            clickListener.onClick(v, getPosition());
        }
    }// class ItemHolder

    public interface ItemClickListener {
        void onClick(View view, int position);
    }
}