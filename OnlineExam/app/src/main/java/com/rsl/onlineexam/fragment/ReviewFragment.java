package com.rsl.onlineexam.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.onlineexam.R;
import com.rsl.onlineexam.activity.ConnectionDetector;
import com.rsl.onlineexam.activity.MainActivity;
import com.rsl.onlineexam.adapter.ReviewRecyclerViewAdapter;
import com.rsl.onlineexam.model.fragmentChangedListner;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ReviewFragment extends Fragment implements fragmentChangedListner {
    // TODO: Rename parameter arguments, choose names that match
    private static SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private Toolbar mToolbar;
    private static Button filter;
    private ConnectionDetector cd;
    static ArrayList<HashMap<String, String>> list_data;
    private static ReviewRecyclerViewAdapter myRecyclerViewAdapter;
    private static RecyclerView recList;
    static final List<String> items = new ArrayList<String>();
    final static ArrayList<String> itemsSelected = new ArrayList<String>();
    static Context c;
    public ReviewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(items.size()==0) {
            items.add("All");
            items.add("Wrong");
            items.add("Bookmarked");
        }
        View rootView = inflater.inflate(R.layout.fragment_review, container, false);
        sp = getActivity().getSharedPreferences("ObjectiveExam", 0);
        editor = sp.edit();
        c = getActivity();
        MainActivity.mToolbar.setVisibility(View.VISIBLE);
        // MainActivity.mToolbar.setTitle("REVIEW");
        filter = (Button) rootView.findViewById(R.id.filter);
        recList = (RecyclerView) rootView.findViewById(R.id.recyclerview_review);
        recList.setHasFixedSize(true);
        list_data = new ArrayList<HashMap<String, String>>();
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        cd = new ConnectionDetector(getActivity().getApplicationContext());
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Dialog dialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                //builder.setTitle("Select Languages you know : ");
               final String s[] = items.toArray(new String[items.size()]);
                final boolean selectedTop[] = new boolean[items.size()];
                for (int i = 0; i < items.size(); i++) {
                    if (itemsSelected.contains(items.get(i))) {
                        selectedTop[i] = true;

                    } else {
                        selectedTop[i] = false;
                    }
                }
              builder.setMultiChoiceItems(s, null,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int selectedItemId,
                                                boolean isSelected) {

                            }
                        })
                        .setPositiveButton("Filter", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                //Your logic when OK button is clicked
                                if(itemsSelected.contains("All")) {
                                    Log.e("itemsSelected ",""+itemsSelected);
                                   itemsSelected.clear();
                                    itemsSelected.addAll(items);
                                }
                                myRecyclerViewAdapter.filterData(itemsSelected);
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                dialog = builder.create();
                dialog.show();
                AlertDialog dialog1 = (AlertDialog) dialog;
                final ListView lv = dialog1.getListView();
                int i = 0;
                while (i < s.length) {
                    if(selectedTop[i])
                        lv.setItemChecked(i, true);
                    else
                        lv.setItemChecked(i, false);
                    i++;
                }
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int selectedItemId, long id) {
                        Log.e("onItemClick", "onItemClick===" + selectedItemId);
                        CheckedTextView v = ((CheckedTextView) view);
                        Log.e("isChecked  ",""+v.isChecked());
                        Log.e("isSelected  ",""+ v.isSelected());
                        boolean isSelected = false;
                        if(v.isChecked() || v.isSelected())
                            isSelected = true;
                        //v.setSelected(true)
                        if (isSelected) {
                            if(selectedItemId==0) {
                                int i = 0;
                                while (i < s.length) {
                                    parent.setSelected(false);
                                    lv.setItemChecked(i, true);
                                    itemsSelected.add(items.get(i));
                                    i++;
                                }
                            }else{
                                //lv.setItemChecked(selectedItemId, true);
                                lv.setItemChecked(selectedItemId, true);
                                itemsSelected.add(items.get(selectedItemId));
                                if(itemsSelected.size()==(items.size()-1)){
                                    itemsSelected.clear();
                                    int i = 0;
                                    while (i < s.length) {
                                        lv.setItemChecked(i, true);
                                        itemsSelected.add(items.get(i));
                                        i++;
                                    }
                                }
                            }

                            Log.e("isSelected if ",""+isSelected);
                        } else {
                            if(selectedItemId==0) {
                                int i = 0;
                                while (i < s.length) {
                                    lv.setItemChecked(i, false);
                                    i++;
                                }
                                itemsSelected.clear();
                            }else{
                                lv.setItemChecked(selectedItemId, false);
                                lv.setItemChecked(0, false);
                                itemsSelected.remove(items.get(0));
                            }
                            Log.e("isSelected else ", "" + isSelected);
                            itemsSelected.remove(items.get(selectedItemId));
                        }
                    }
                });


                //filterDialog();
            }
        });
        if (cd.isConnectingToInternet()) {
            MakeAPICall();
        } else {
            cd.showAlertDialog(getActivity(), "No Internet Connection",
                    "You don't have internet connection.", false);
        }
        return rootView;
    }

    private void filterDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Set the adapter
        builder.setAdapter(
                new ArrayAdapter<String>(getActivity(),
                        R.layout.dialog_listitem   , items), null)
                // Set the action buttons
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });

        AlertDialog alertDialog = builder.create();

        ListView listView = alertDialog.getListView();
        listView.setAdapter(new ArrayAdapter<String>(getActivity(),
                R.layout.dialog_listitem, items));
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ((CheckedTextView) view).setChecked(true);
               // else
                 //   checkedTextView.setChecked(false);
                //checkedItems[position] = !checkedTextView.isChecked();
            }
        });
        listView.setDivider(null);
        listView.setDividerHeight(-1);

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {
               // setCheckedItems(((AlertDialog) dialog).getListView());
            }
        });

        alertDialog.show();
    }

    public static void MakeAPICall() {
        itemsSelected.clear();
        items.clear();
        items.add("All");
        items.add("Wrong");
        items.add("Bookmarked");
        getAlldata(sp.getString("SubjectId", ""), sp.getString("students_id", ""));
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.mTitle.setText("Review");
        MainActivity.currentF = "review";
        MainActivity.item.setVisible(false);
        c = getActivity();
        // MainActivity.mToolbar.setTitle("REVIEW");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private static void getAlldata(final String subId, final String studentsId) {
        list_data.clear();
        final ProgressDialog dialog = new ProgressDialog(c, R.style.MyThemeHoloDialog);
        dialog.setCancelable(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(c);
        String requestURL = c.getResources().getString(R.string.web_service_url)+"practice_review.php";
        JSONObject obj = new JSONObject();
        try {
            obj.put("subject_id", subId);
            obj.put("students_id", studentsId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("JSONObject ", "" + obj.toString()+""+requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(",", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.e("onResponse ", s);
                if (s != null) {
                    if (!s.startsWith("null")) {
                        try {
                            JSONObject jObj = new JSONObject(s);
                            if (jObj.getString("result").equals("success")) {
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                                if (jObj.getJSONArray("data").length() > 0) {
                                    filter.setEnabled(true);
                                    for (int a = 0; a < jObj.getJSONArray("data")
                                            .length(); a++) {
                                        JSONObject b = jObj.getJSONArray("data")
                                                .getJSONObject(a);
                                        HashMap<String, String> map = new HashMap<String, String>();
                                        map.put("topic_id", b.getString("topic_id"));
                                        map.put("topic_name", b.getString("topic_name"));
                                        map.put("question_id", b.getString("question_id"));
                                        map.put("question", b.getString("question"));
                                        map.put("answer_id", b.getString("answer_id"));
                                        map.put("answer", b.getString("answer"));
                                        map.put("bookmark_flag", b.getString("bookmark_flag"));
                                        map.put("answer_flag", b.getString("answer_flag"));
                                        list_data.add(map);
                                        String tn = map.get("topic_name");
                                        if (!items.contains(tn))
                                            items.add(tn);
                                    }
                                    myRecyclerViewAdapter = new ReviewRecyclerViewAdapter(c, list_data);
                                    recList.setAdapter(myRecyclerViewAdapter);
                                    myRecyclerViewAdapter.notifyDataSetChanged();
                                } else {
                                    filter.setEnabled(false);
                                    myRecyclerViewAdapter = new ReviewRecyclerViewAdapter(c, list_data);
                                    recList.setAdapter(myRecyclerViewAdapter);
                                    myRecyclerViewAdapter.notifyDataSetChanged();
                                }
                            } else if (jObj.getString("result")
                                    .equals("failed")) {
                                // alertDialog(jObj.getString("No School List"));
                                dialog.dismiss();
                                filter.setEnabled(false);
                                myRecyclerViewAdapter = new ReviewRecyclerViewAdapter(c, list_data);
                                recList.setAdapter(myRecyclerViewAdapter);
                                myRecyclerViewAdapter.notifyDataSetChanged();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    } else {
                        dialog.dismiss();
                        filter.setEnabled(false);
                        myRecyclerViewAdapter = new ReviewRecyclerViewAdapter(c, list_data);
                        recList.setAdapter(myRecyclerViewAdapter);
                        myRecyclerViewAdapter.notifyDataSetChanged();
                    }
                } else {
                    filter.setEnabled(false);
                    dialog.dismiss();
                    myRecyclerViewAdapter = new ReviewRecyclerViewAdapter(c, list_data);
                    recList.setAdapter(myRecyclerViewAdapter);
                    myRecyclerViewAdapter.notifyDataSetChanged();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("subject_id", subId);
                params.put("students_id", studentsId);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(stringRequest);
    }

    @Override
    public void changed() {
        Log.e("errrrrrorrrrrrr", "onDrawerItemSelected");
        if (MainActivity.fragmentFlag) {
            Log.e("errrrrrorrrrrrr", "reload fragment here");
            if (cd.isConnectingToInternet()) {
                getAlldata(sp.getString("SubjectId", ""), sp.getString("students_id", ""));
            } else {
                cd.showAlertDialog(getActivity(), "No Internet Connection",
                        "You don't have internet connection.", false);
            }
            MainActivity.fragmentFlag = false;
        }
    }
}