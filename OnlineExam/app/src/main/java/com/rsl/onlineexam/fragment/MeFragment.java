package com.rsl.onlineexam.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.onlineexam.R;
import com.rsl.onlineexam.activity.ConnectionDetector;
import com.rsl.onlineexam.activity.LoginActivity;
import com.rsl.onlineexam.activity.MainActivity;
import com.rsl.onlineexam.model.RoundImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;


public class MeFragment extends Fragment
{
    // TODO: Rename parameter arguments, choose names that match
    private SharedPreferences sp;
    SharedPreferences.Editor editor;
    private TextView questions_done;
    private TextView best_topic;
    private ImageView user_image;
    private Button logout;
    private ConnectionDetector cd;
    private  RoundImage roundedImage;
    private  Bitmap bitmap;
    public MeFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rooView= inflater.inflate(R.layout.fragment_me, container, false);
        sp = getActivity().getSharedPreferences("ObjectiveExam", 0);
        editor = sp.edit();
        MainActivity.mToolbar.setVisibility(View.GONE);
        MainActivity.mTitle.setText(sp.getString("student_name",""));
        logout = (Button)rooView.findViewById(R.id.logout_in_button);
        questions_done = (TextView)rooView.findViewById(R.id.question_done);
        best_topic = (TextView)rooView.findViewById(R.id.best_topic);
        user_image = (ImageView)rooView.findViewById(R.id.imageView1);
        cd = new ConnectionDetector(getActivity().getApplicationContext());
        if(sp.getString("student_image","").equals("")){
            Log.e("Student image if", sp.getString("student_image", ""));
            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_profile);
            roundedImage = new RoundImage(bm);
            user_image.setImageDrawable(roundedImage);
        }else {
            String pimg =getResources().getString(R.string.web_service_url)
                    + sp.getString("student_image","");
            Log.e("Student image else", pimg);
            //Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_profile);
            new LoadImage().execute(pimg);
        }
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("logout", "logout");
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle("Confirm");
                alert.setIcon(R.mipmap.ic_launcher);
                alert.setMessage("Are you sure you want to Logout?");
                alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.clear();
                        editor.commit();
                        Intent logout = new Intent(getActivity(),LoginActivity.class);
                        startActivity(logout);
                        getActivity().finish();
                    }
                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.create().show();
            }
        });
       // getQuestionsDone();
        if (cd.isConnectingToInternet()) {
            getQuestionsDone(sp.getString("SubjectId", ""), sp.getString("students_id",""));
        } else {
            cd.showAlertDialog(getActivity(), "No Internet Connection",
                    "You don't have internet connection.", false);
        }
        return  rooView;
    }
    private class LoadImage extends AsyncTask<String, String, Bitmap> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream)new URL(args[0]).getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {

            if(image != null){
                roundedImage = new RoundImage(image);
                user_image.setImageDrawable(roundedImage);
            }else{
                Toast.makeText(getActivity(), "Image Does Not exist or Network Error", Toast.LENGTH_SHORT).show();
            }
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        //MainActivity.mToolbar.setTitle(sp.getString("student_name",""));
        MainActivity.mTitle.setText(sp.getString("student_name",""));
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }
    @Override
    public void onDetach() {
        super.onDetach();
    }

    private  void getQuestionsDone(final String subId,final  String studentsId)
    {
        final ProgressDialog dialog = new ProgressDialog(getActivity(),R.style.MyThemeHoloDialog);
        //dialog.setTitle("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(getActivity());
        String requestURL =getResources().getString(R.string.web_service_url)+"best_topic.php";
        JSONObject obj = new JSONObject();
        try {
            obj.put("students_id",studentsId);
            obj.put("subject_id", subId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("JSONObject ", "" + obj.toString());
        StringRequest stringRequest = new StringRequest(Request.Method.POST,requestURL.replaceAll(" ","%20"),new Response.Listener<String>()
        {
            @Override
            public void onResponse(String s)
            {
                Log.e("onResponse ",s);
                if (s != null) {
                    if (!s.startsWith("null")) {
                        try {
                            JSONObject jObj = new JSONObject(s);
                            if (jObj.length() > 0) {
                                if (jObj.getString("result").equals("success"))
                                {
                                    questions_done.setText("Questions done"+":"+jObj.getString("question_attempted"));
                                    best_topic.setText("Best Topic:"+jObj.getString("best_topic"));
                                    if (dialog.isShowing()) {
                                        dialog.dismiss();
                                    }
                                } else
                                    dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    } else {
                        dialog.dismiss();
                    }
                } else {
                    alertDialog("Please try again");
                    dialog.dismiss();
                }
            }
        },new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }){
            @Override
            protected Map<String, String> getParams () {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("students_id",studentsId);
                params.put("subject_id", subId);
                Log.e("params ", "" + params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders ()throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(stringRequest);
    }
    private void alertDialog(String errer) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                getActivity());
        alertDialog.setTitle("OnlineExam");
        alertDialog.setMessage(errer);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                    }
                });
        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                getActivity().finish();
            }
        });
        alertDialog.show();
    }
}
