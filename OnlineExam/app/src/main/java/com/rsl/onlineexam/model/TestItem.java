package com.rsl.onlineexam.model;

/**
 * Created by admin on 11/19/2015.
 */
public class TestItem
{
    private String testId;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    private String time;

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    private String testName;

    public String getDone() {
        return done;
    }

    public void setDone(String done) {
        this.done = done;
    }

    private String done;

    public TestItem(){

    }
    public TestItem(String testName, String testId,String done,String time) {
        this.testName = testName;
        this.testId = testId;
        this.done = done;
        this.time = time;
    }
}
