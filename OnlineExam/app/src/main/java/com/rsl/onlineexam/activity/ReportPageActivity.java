package com.rsl.onlineexam.activity;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.onlineexam.R;
import com.rsl.onlineexam.adapter.ReportRecyclerViewAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class ReportPageActivity extends AppCompatActivity {
    private Toolbar mToolbar;
    public static ArrayList<HashMap<String, String>> Practice_display_data;
    ReportRecyclerViewAdapter myRecyclerViewAdapter;
    private RecyclerView recList;
    public static boolean flag = false;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private ConnectionDetector cd;
    private TextView mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_page);
        sp = getSharedPreferences("ObjectiveExam", 0);
        editor = sp.edit();
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        //MainActivity.mToolbar.setTitle("REPORT");
        mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("Report");
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        recList = (RecyclerView) findViewById(R.id.cardList);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        cd = new ConnectionDetector(getApplicationContext());
        Practice_display_data = new ArrayList<HashMap<String, String>>();
        if (getIntent().getStringExtra("activity").equals("PracticePage")) {

            Intent intent = getIntent();
            String jsonArray = intent.getStringExtra("jsonArray");
            Log.e("JsonArray", "" + jsonArray);
            try {
                //  JSONArray array = new JSONArray(jsonArray);
                JSONObject job = new JSONObject(jsonArray);
                if (job.getJSONArray("data").length() > 0) {
                    for (int a = 0; a < job.getJSONArray("data").length(); a++) {
                        HashMap<String, String> map = new HashMap<String, String>();
                        JSONObject b = job.getJSONArray("data").getJSONObject(a);
                        map.put("Explanation", b.getString("Explanation"));
                        map.put("Check", b.getString("Check"));
                        map.put("Ans_option", b.getString("Ans_option"));
                        map.put("CorrectAns", b.getString("CorrectAns"));
                        map.put("question", b.getString("question"));
                        map.put("option_id", b.getString("option_id"));
                        map.put("question_id", b.getString("question_id"));
                        map.put("questionNo", b.getString("questionNo"));
                        Practice_display_data.add(map);
                        Log.e("come from", "PracticePAge");
                        flag = true;
                        myRecyclerViewAdapter = new ReportRecyclerViewAdapter(ReportPageActivity.this, Practice_display_data);
                        recList.setAdapter(myRecyclerViewAdapter);
                        // Log.e("pra_list", "" + Practice_display_data);
                    }//for
                }//if
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (getIntent().getStringExtra("activity").equals("TestPage")) {
            Log.e("come from", "TestPage");
            Intent intent = getIntent();
            String jsonArray = intent.getStringExtra("jsonArray");
            Log.e("JsonArray", "" + jsonArray);
            try {
                //  JSONArray array = new JSONArray(jsonArray);
                JSONObject job = new JSONObject(jsonArray);
                if (job.getJSONArray("data").length() > 0) {

                    for (int a = 0; a < job.getJSONArray("data").length(); a++) {
                        HashMap<String, String> map = new HashMap<String, String>();
                        JSONObject b = job.getJSONArray("data").getJSONObject(a);
                        map.put("Explanation", b.getString("Explanation"));
                        map.put("Check", b.getString("Check"));
                        map.put("Ans_option", b.getString("Ans_option"));
                        map.put("CorrectAns", b.getString("CorrectAns"));
                        map.put("question", b.getString("question"));
                        map.put("option_id", b.getString("option_id"));
                        map.put("question_id", b.getString("question_id"));
                        map.put("questionNo", b.getString("questionNo"));
                        Practice_display_data.add(map);
                        Log.e("come from", "Testage");
                        flag = false;
                        myRecyclerViewAdapter = new ReportRecyclerViewAdapter(ReportPageActivity.this, Practice_display_data);
                        recList.setAdapter(myRecyclerViewAdapter);
                    }//for
                }//if
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//onCreate
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_report, menu);
        MenuItem item = menu.findItem(R.id.action_done);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_done:
                if (getIntent().getStringExtra("activity").equals("PracticePage")) {
                    if (cd.isConnectingToInternet()) {
                        if (ReportRecyclerViewAdapter.bookmarkList.isEmpty()) {
                            alertConfirmDiloagPracticePage("Are you sure want submit without bookmark?");
                        } else {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                                    ReportPageActivity.this);
                            alertDialog.setTitle("Confirm");
                            alertDialog.setMessage("Are you sure want to submit bookmark?");
                            alertDialog.setCancelable(false);
                            alertDialog.setIcon(R.mipmap.ic_launcher);
                            alertDialog.setPositiveButton("YES",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            // TODO Auto-generated method stub
                                            String bookmark_id = "";
                                            for (int i = 0; i < ReportRecyclerViewAdapter.bookmarkList.size(); i++) {
                                                if (bookmark_id.equals("")) {
                                                    bookmark_id = ReportRecyclerViewAdapter.bookmarkList.get(i);
                                                } else {
                                                    bookmark_id += "," + ReportRecyclerViewAdapter.bookmarkList.get(i);
                                                }
                                            }
                                            Log.e("TopicRecycler for", bookmark_id);
                                            practiceBookmarkSave(sp.getString("students_id", ""), sp.getString("SubjectId", ""), bookmark_id);
                                        }
                                    });
                            alertDialog.setNegativeButton("NO",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            // TODO Auto-generated method stub
                                            dialog.dismiss();
                                        }
                                    });
                            alertDialog.show();
                        }
                    } else {
                        cd.showAlertDialog(ReportPageActivity.this, "No Internet Connection",
                                "You don't have internet connection.", false);
                    }
                } else if (getIntent().getStringExtra("activity").equals("TestPage")) {
                    if (cd.isConnectingToInternet()) {
                        if (ReportRecyclerViewAdapter.bookmarkList.isEmpty()) {
                            alertConfirmDiloagTestPage("Are you sure want submit without bookmark?");
                        } else {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                                    ReportPageActivity.this);
                            alertDialog.setTitle("Confirm");
                            alertDialog.setMessage("Are you sure want to submit bookmark?");
                            alertDialog.setCancelable(false);
                            alertDialog.setIcon(R.mipmap.ic_launcher);
                            alertDialog.setPositiveButton("YES",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            // TODO Auto-generated method stub
                                            String bookmark_id = "";
                                            for (int i = 0; i < ReportRecyclerViewAdapter.bookmarkList.size(); i++) {
                                                if (bookmark_id.equals("")) {
                                                    bookmark_id = ReportRecyclerViewAdapter.bookmarkList.get(i);
                                                } else {
                                                    bookmark_id += "," + ReportRecyclerViewAdapter.bookmarkList.get(i);
                                                }
                                            }
                                            Log.e("TopicRecycler for", bookmark_id);
                                            testBookmarkSave(sp.getString("students_id", ""), bookmark_id);
                                        }
                                    });
                            alertDialog.setNegativeButton("NO",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            // TODO Auto-generated method stub
                                            dialog.dismiss();
                                        }
                                    });
                            alertDialog.show();
                        }
                    } else {
                        cd.showAlertDialog(ReportPageActivity.this, "No Internet Connection",
                                "You don't have internet connection.", false);
                    }
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getStringExtra("activity").equals("PracticePage")) {
            // Log.e("come from", "PracticePAge");
            if (cd.isConnectingToInternet()) {
                if (ReportRecyclerViewAdapter.bookmarkList.isEmpty()) {
                    alertConfirmDiloagPracticePage("Are you sure want to go back?");
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                            ReportPageActivity.this);
                    alertDialog.setTitle("Confirm");
                    alertDialog.setMessage("Do you want to submit bookmark?");
                    alertDialog.setCancelable(false);
                    alertDialog.setIcon(R.mipmap.ic_launcher);
                    alertDialog.setPositiveButton("YES",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // TODO Auto-generated method stub
                                    String bookmark_id = "";
                                    for (int i = 0; i < ReportRecyclerViewAdapter.bookmarkList.size(); i++) {
                                        if (bookmark_id.equals("")) {
                                            bookmark_id = ReportRecyclerViewAdapter.bookmarkList.get(i);
                                        } else {
                                            bookmark_id += "," + ReportRecyclerViewAdapter.bookmarkList.get(i);
                                        }
                                    }
                                    Log.e("TopicRecycler for", bookmark_id);
                                    practiceBookmarkSave(sp.getString("students_id", ""), sp.getString("SubjectId", ""), bookmark_id);
                                }
                            });
                    alertDialog.setNegativeButton("NO",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // TODO Auto-generated method stub
                                   dialog.dismiss();
                                    finish();
                                }
                            });
                    alertDialog.show();
                }
            } else {
                cd.showAlertDialog(ReportPageActivity.this, "No Internet Connection",
                        "You don't have internet connection.", false);
            }
        } else if (getIntent().getStringExtra("activity").equals("TestPage")) {
            if (cd.isConnectingToInternet()) {
                if (ReportRecyclerViewAdapter.bookmarkList.isEmpty()) {
                    alertConfirmDiloagTestPage("Are you sure want to go back?");
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                            ReportPageActivity.this);
                    alertDialog.setTitle("Confirm");
                    alertDialog.setMessage("Do you want to submit bookmark?");
                    alertDialog.setCancelable(false);
                    alertDialog.setIcon(R.mipmap.ic_launcher);
                    alertDialog.setPositiveButton("YES",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // TODO Auto-generated method stub
                                    String bookmark_id = "";
                                    for (int i = 0; i < ReportRecyclerViewAdapter.bookmarkList.size(); i++) {
                                        if (bookmark_id.equals("")) {
                                            bookmark_id = ReportRecyclerViewAdapter.bookmarkList.get(i);
                                        } else {
                                            bookmark_id += "," + ReportRecyclerViewAdapter.bookmarkList.get(i);
                                        }
                                    }
                                    Log.e("TopicRecycler for", bookmark_id);
                                    testBookmarkSave(sp.getString("students_id", ""), bookmark_id);
                                }
                            });
                    alertDialog.setNegativeButton("NO",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // TODO Auto-generated method stub
                                    dialog.dismiss();
                                    finish();
                                }
                            });
                    alertDialog.show();
                }
            } else {
                cd.showAlertDialog(ReportPageActivity.this, "No Internet Connection",
                        "You don't have internet connection.", false);
            }
        }
    }//onBackPressed

    @Override
    protected void onResume() {
        //MainActivity.mToolbar.setTitle("REPORT");
        mTitle.setText("Report");
        super.onResume();
    }
    private void practiceBookmarkSave(final String students_id, final String sub, final String question_id) {
        // final ProgressDialog dialog = new ProgressDialog(ReportPageActivity.this,R.style.MyThemeHoloDialog);
        //dialog.setTitle("Load
        // ing...");
        //  dialog.setCancelable(false);
        // dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(ReportPageActivity.this);
        String requestURL = getResources().getString(R.string.web_service_url)+"practice_bookmarked.php";
        JSONObject obj = new JSONObject();
        try {
            obj.put("students_id", students_id);
            obj.put("question_id", question_id);
            obj.put("sub_id", sub);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.e("ResponseBookmark ", s);
                if (s != null) {
                    if (!s.startsWith("null")) {
                        try {
                            JSONObject jObj = new JSONObject(s);
                            if (jObj.length() > 0) {
                                if (jObj.getString("result").equals("success")) {

                                    finish();
                                }
                                // dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            // dialog.dismiss();
                        }
                    } else {
                        // dialog.dismiss();
                    }
                } else {
                    alertDialog("Please try again");
                    //  dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("student_id", students_id);
                params.put("question_id", question_id.toString());
                params.put("sub_id", sub);
                Log.e("ParaBookmarkJSONObject ", "" + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(stringRequest);
    }

    private void testBookmarkSave(final String students_id, final String question_id) {
        // final ProgressDialog dialog = new ProgressDialog(ReportPageActivity.this,R.style.MyThemeHoloDialog);
        //dialog.setTitle("Loading...");
        //  dialog.setCancelable(false);
        // dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(ReportPageActivity.this);
        String requestURL = getResources().getString(R.string.web_service_url)+"test_bookmarked.php";
        JSONObject obj = new JSONObject();
        try {
            obj.put("students_id", students_id);
            obj.put("question_id", question_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.e("ResponseBookmark ", s);
                if (s != null) {
                    if (!s.startsWith("null")) {
                        try {
                            JSONObject jObj = new JSONObject(s);
                            if (jObj.length() > 0) {
                                if (jObj.getString("result").equals("success")) {
                                    MainActivity.flag=true;
                                  finish();
                                }
                                // dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            // dialog.dismiss();
                        }
                    } else {
                        // dialog.dismiss();
                    }
                } else {
                    alertDialog("Please try again");
                    //  dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("student_id", students_id);
                params.put("question_id", question_id.toString());
                Log.e("ParaBookmarkJSONObject ", "" + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(stringRequest);
    }

    private void alertDialog(String errer) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                ReportPageActivity.this);
        alertDialog.setTitle("OnlineExam");
        alertDialog.setMessage(errer);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                    }
                });
        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                ReportPageActivity.this.finish();
            }
        });
        alertDialog.show();
    }


    private void alertConfirmDiloagTestPage(String error) {
        AlertDialog.Builder alert = new AlertDialog.Builder(ReportPageActivity.this);
        alert.setTitle("Confirm");
        alert.setIcon(R.mipmap.ic_launcher);
        alert.setMessage(error);
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                MainActivity.flag=true;
                dialog.dismiss();
                finish();
            }
        });
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert.create().show();
    }
    private void alertConfirmDiloagPracticePage(String error) {
        AlertDialog.Builder alert = new AlertDialog.Builder(ReportPageActivity.this);
        alert.setTitle("Confirm");
        alert.setIcon(R.mipmap.ic_launcher);
        alert.setMessage(error);
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Intent back = new Intent(ReportPageActivity.this, MainActivity.class);
              //  startActivity(back);
                finish();
            }
        });
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert.create().show();
    }
}