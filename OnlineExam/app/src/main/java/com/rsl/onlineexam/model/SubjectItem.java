package com.rsl.onlineexam.model;

/**
 * Created by admin on 11/6/2015.
 */
public class SubjectItem
{
    private String subjectId;
    private String subjectName;

    public SubjectItem(){

    }
    public SubjectItem(String subjectName, String subjectId) {
        this.subjectName = subjectName;
        this.subjectId = subjectId;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }



}
