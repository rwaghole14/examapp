package com.rsl.onlineexam.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.onlineexam.R;
import com.rsl.onlineexam.activity.ConnectionDetector;
import com.rsl.onlineexam.activity.MainActivity;
import com.rsl.onlineexam.adapter.TestRecyclerViewAdapter;
import com.rsl.onlineexam.model.TestItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class TestFragment extends Fragment
{
    private static SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private Toolbar mToolbar;
    private static TestRecyclerViewAdapter myRecyclerViewAdapter;
    private static RecyclerView recList;
    private static ArrayList<TestItem> list_data=list_data = new ArrayList<TestItem>();;
    private static Activity c;
    private ConnectionDetector cd;
    public TestFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView= inflater.inflate(R.layout.fragment_test, container, false);
        sp = getActivity().getSharedPreferences("ObjectiveExam", 0);
        editor = sp.edit();
        c=getActivity();
        //MainActivity.mToolbar.setTitle("TESTS");
        MainActivity.mTitle.setText("Tests");
        recList = (RecyclerView)rootView.findViewById(R.id.cardList);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);

        cd = new ConnectionDetector(getActivity());
        if (cd.isConnectingToInternet()) {
            getAlltest();
        } else {
            cd.showAlertDialog(getActivity(), "No Internet Connection",
                    "You don't have internet connection.", false);
        }
        return  rootView;
    }
    @Override
    public void onResume() {
        super.onResume();
       // MainActivity.mToolbar.setTitle("TESTS");
        MainActivity.currentF = "test";
        MainActivity.mTitle.setText("Tests");
        c=getActivity();
        MainActivity.item.setVisible(false);
        MainActivity.mToolbar.setVisibility(View.VISIBLE);
    }
    public static void getAlltest() {

        final String subId = sp.getString("SubjectId", "");
        final  String studentsId = sp.getString("students_id", "");
        final ProgressDialog dialog = new ProgressDialog(c,R.style.MyThemeHoloDialog);
        // dialog.setTitle("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(c);
        String requestURL =c.getResources().getString(R.string.web_service_url)+"getTest_bySubject.php";
        JSONObject obj = new JSONObject();
        try {
            obj.put("sub_id", subId);
            obj.put("student_id", studentsId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("JSONObject ", "" + obj.toString()+""+requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(",", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.e("onResponse ",s);
                if (s != null) {
                    if (!s.startsWith("null")) {
                        try {
                            JSONObject jObj = new JSONObject(s);
                            if (jObj.getString("result").equals("success")) {
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                                if (jObj.getJSONArray("data").length() > 0) {
                                    list_data.clear();
                                    for (int a = 0; a < jObj.getJSONArray("data")
                                            .length(); a++) {
                                        JSONObject b = jObj.getJSONArray("data")
                                                .getJSONObject(a);
                                        TestItem data = new TestItem();
                                        data.setTestId(b.getString("test_id"));
                                        data.setTestName(b.getString("test_name"));
                                        data.setDone(b.getString("flag"));
                                        //data.setTime(b.getString("time"));
                                        list_data.add(data);
                                    }
                                    myRecyclerViewAdapter = new TestRecyclerViewAdapter(c, list_data);
                                    recList.setAdapter(myRecyclerViewAdapter);
                                    myRecyclerViewAdapter.notifyDataSetChanged();
                                }
                            } else if (jObj.getString("result")
                                    .equals("failed")) {
                                // alertDialog(jObj.getString("No School List"));
                                dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    } else {
                        dialog.dismiss();
                    }
                } else {
                    dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }){
            @Override
            protected Map<String, String> getParams () {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("sub_id", subId);
                params.put("student_id", studentsId);
                return params;
            }
            @Override
            public Map<String, String> getHeaders ()throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(stringRequest);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }
}
