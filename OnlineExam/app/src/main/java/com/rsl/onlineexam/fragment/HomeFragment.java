package com.rsl.onlineexam.fragment;

/**
 * Created by Ravi on 29/07/15.
 */

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;

import com.rsl.onlineexam.R;
import com.rsl.onlineexam.activity.MainActivity;

public class HomeFragment extends Fragment {
    private FragmentTabHost mTabHost;
    String strtext;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        // Inflate the layout for this fragment
        strtext = getArguments().getString("CID");

        mTabHost = (FragmentTabHost) rootView.findViewById(android.R.id.tabhost);
        mTabHost.setup(getActivity(), getChildFragmentManager(), R.id.realtabcontent);
        Bundle b = new Bundle();
        b.putString("key", "Simple");
        View v = inflater.inflate(R.layout.tab_selector,null,false);
        ((ImageView)v.findViewById(R.id.img_tab_icon)).setImageResource(R.drawable.ic_practice);
        ((TextView)v.findViewById(R.id.txt_tab)).setText("Practice");
        mTabHost.addTab(mTabHost.newTabSpec("tab1").setIndicator(v),
                PracticeFragment.class, b);
        View v1 = inflater.inflate(R.layout.tab_selector,null,false);
        ((ImageView)v1.findViewById(R.id.img_tab_icon)).setImageResource(R.drawable.ic_test);
        ((TextView)v1.findViewById(R.id.txt_tab)).setText("Test");
        mTabHost.addTab(mTabHost.newTabSpec("tab2").setIndicator(v1),
                TestFragment.class, b);
        View v2 = inflater.inflate(R.layout.tab_selector,null,false);
        ((ImageView)v2.findViewById(R.id.img_tab_icon)).setImageResource(R.drawable.ic_review);
        ((TextView)v2.findViewById(R.id.txt_tab)).setText("Review");
        mTabHost.addTab(mTabHost.newTabSpec("tab3").setIndicator(v2),
                ReviewFragment.class, b);
        View v3 = inflater.inflate(R.layout.tab_selector,null,false);
        ((ImageView)v3.findViewById(R.id.img_tab_icon)).setImageResource(R.drawable.ic_profile);
        ((TextView)v3.findViewById(R.id.txt_tab)).setText("Me");
        mTabHost.addTab(mTabHost.newTabSpec("tab4").setIndicator(v3),
                MeFragment.class, b);

        for(int i=0;i<mTabHost.getTabWidget().getChildCount();i++)
        {
            mTabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.parseColor("#29abe2")); //unselected
            if(mTabHost.getCurrentTab()==0)
                mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab()).setBackgroundColor(Color.parseColor("#0b95cf")); //1st tab selected
            else
                mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab()).setBackgroundColor(Color.parseColor("#0b95cf"));
        }

        if(strtext!=""){
            mTabHost.getTabWidget().getChildAt(1).setBackgroundColor(Color.parseColor("#0b95cf"));
            mTabHost.getTabWidget().getChildAt(0).setBackgroundColor(Color.parseColor("#29abe2"));
           mTabHost.setCurrentTab(1);
            //MainActivity.flag=false;
        }

        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                for(int i=0;i<mTabHost.getTabWidget().getChildCount();i++)
                    mTabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.parseColor("#29abe2")); //unselected

                if(mTabHost.getCurrentTab()==0){
                    mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab()).setBackgroundColor(Color.parseColor("#0b95cf")); //1st tab selected
                    if(MainActivity.item != null)
                    MainActivity.item.setVisible(true);
                }

                else {
                    mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab()).setBackgroundColor(Color.parseColor("#0b95cf")); //2nd tab selected
                    MainActivity.item.setVisible(false);
                }
                Log.d("==========="+mTabHost.getCurrentTab(),"============"+tabId);
            }

        });
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
