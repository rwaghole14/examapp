package info.androidhive.onlineexam.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.onlineexam.R;

import org.json.JSONObject;

import java.util.ArrayList;

import info.androidhive.onlineexam.adapter.MyRecyclerViewAdapter;
import info.androidhive.onlineexam.model.SubjectItem;

/**
 * Created by admin on 10/31/2015.
 */
public class SelectSubjectActivity extends AppCompatActivity
{
    private MyRecyclerViewAdapter myRecyclerViewAdapter;
    private  RecyclerView recList;
    ArrayList<SubjectItem> list_data;
    private ConnectionDetector cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_subject);
        recList = (RecyclerView) findViewById(R.id.cardList);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        list_data = new ArrayList<SubjectItem>();
        cd = new ConnectionDetector(getApplicationContext());
        if (cd.isConnectingToInternet()) {
            getAllsubject();
        } else {
            cd.showAlertDialog(SelectSubjectActivity.this, "No Internet Connection",
                    "You don't have internet connection.", false);
        }
    }
    private void getAllsubject(){
        final ProgressDialog dialog = new ProgressDialog(SelectSubjectActivity.this);
        dialog.setTitle("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(SelectSubjectActivity.this);
        String requestURL ="http://rslinfotech.in/ObjectiveExam/Ws/getAll_subject.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(",","%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String s)
            {
                if (s != null) {
                    if (!s.startsWith("null")) {
                        try {
                            JSONObject jObj = new JSONObject(s);
                            if (jObj.getString("result").equals("success")) {
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                                if (jObj.getJSONArray("data").length() > 0) {
                                    for (int a = 0; a < jObj.getJSONArray("data")
                                            .length(); a++) {
                                        JSONObject b = jObj.getJSONArray("data")
                                                .getJSONObject(a);
                                        SubjectItem data = new SubjectItem();
                                        data.setSubjectId(b.getString("subject_id"));
                                        data.setSubjectName(b.getString("subject_name"));
                                        list_data.add(data);
                                    }
                                    myRecyclerViewAdapter = new MyRecyclerViewAdapter(SelectSubjectActivity.this,list_data);
                                    recList.setAdapter(myRecyclerViewAdapter);
                                }
                            } else if (jObj.getString("result")
                                    .equals("failed")) {
                               // alertDialog(jObj.getString("No School List"));
                                dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    } else {
                        dialog.dismiss();
                    }
                } else {
                    dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
            }
        });
        queue.add(stringRequest);
    }
}
