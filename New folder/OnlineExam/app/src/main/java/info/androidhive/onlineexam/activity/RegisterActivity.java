package info.androidhive.onlineexam.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.onlineexam.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import info.androidhive.onlineexam.model.RoundImage;

/**
 * Created by admin on 10/31/2015.
 */
public class RegisterActivity extends AppCompatActivity implements OnClickListener {
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    Button reg;
    String email, password, con_password,schoolId,name;
    EditText isEmEditText, isPEditText, isCnEdtiText,isNaEditText;
    Spinner school;
    ImageView imageView1;
    RoundImage roundedImage;
    HashMap<String, String> data = null;
    ArrayAdapter<String> adapter;
    private ArrayList<HashMap<String, String>> list_data;
    ArrayList<String> schoolname_Array = new ArrayList<String>();
    private ConnectionDetector cd;
    private static final int SELECT_PHOTO = 786;
    String encodedImage = "";
    private static final String TEMP_PHOTO_FILE = "temporary_holder.jpg";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        sp = getSharedPreferences("ObjectiveExam", 0);
        editor = sp.edit();
        imageView1 = (ImageView) findViewById(R.id.imageView1);
        isNaEditText = (EditText)findViewById(R.id.name);
        reg = (Button) findViewById(R.id.regi_in_button);
        isEmEditText = (EditText) findViewById(R.id.email);
        isPEditText = (EditText) findViewById(R.id.password);
        isCnEdtiText = (EditText) findViewById(R.id.con_password);
        school = (Spinner) findViewById(R.id.school);
        list_data = new ArrayList<HashMap<String, String>>();
        data = new HashMap<String, String>();
        reg.setOnClickListener(this);
        cd = new ConnectionDetector(getApplicationContext());
        if (cd.isConnectingToInternet()) {
            getSchool();
        } else {
            cd.showAlertDialog(RegisterActivity.this, "No Internet Connection",
                    "You don't have internet connection.", false);
        }
        imageView1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);

                photoPickerIntent.setType("image/*");
                photoPickerIntent.putExtra("crop", "true");
                photoPickerIntent.putExtra("outputX", 200);
                photoPickerIntent.putExtra("outputY", 200);
                photoPickerIntent.putExtra("aspectX", 1);
                photoPickerIntent.putExtra("aspectY", 1);
                photoPickerIntent.putExtra("scale", true);
                photoPickerIntent.putExtra(MediaStore.EXTRA_OUTPUT, getTempUri());
                photoPickerIntent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
                startActivityForResult(photoPickerIntent, SELECT_PHOTO);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case SELECT_PHOTO:
                if (resultCode == RESULT_OK) {
//                    Uri selectedImage = imageReturnedIntent.getData();
                    //   InputStream imageStream =  getActivity().getContentResolver().openInputStream(selectedImage);
                    if (imageReturnedIntent != null) {
                        File tempFile = getTempFile();
                        String filePath = Environment.getExternalStorageDirectory()
                                + "/" + TEMP_PHOTO_FILE;
                        System.out.println("path " + filePath);
                        Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
                        imageView1.setImageBitmap(selectedImage);
                        roundedImage = new RoundImage(selectedImage);
                        imageView1.setImageDrawable(roundedImage);
                        encodedImage = encodeTobase64(selectedImage);
                        //postUsingVolley();
                        if (tempFile.exists()) tempFile.delete();
                    }
//                        Bitmap yourSelectedImage = decodeUri(selectedImage);
//                        encodedImage = encodeTobase64(yourSelectedImage);
//                        Log.i("encodedImage", "----" + encodedImage);
//
////                        yourSelectedImage=decodeBase64(encodedImage);
//
//                        img_group.setImageBitmap(yourSelectedImage);
                }
        }
    }

    private Uri getTempUri() {
        return Uri.fromFile(getTempFile());
    }

    private File getTempFile() {

        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File file = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE);
            try {
                file.createNewFile();
            } catch (IOException e) {
            }
            return file;
        } else {

            return null;
        }
    }

    private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {

        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o);
        // The new size we want to scale to
        final int REQUIRED_SIZE = 140;
        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE
                    || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }
        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o2);
    }

    public static String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        Log.e("LOOK", imageEncoded);
        return imageEncoded;
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }
    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    // Email Validation
    public boolean validateEmail(String email) {
        Pattern pattern;
        Matcher matcher;
        String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 8;
    }

    private boolean isConPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.equals(password);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.regi_in_button:
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                Log.e("REg Button click", "REg Button click");
                name = isNaEditText.getText().toString();
                email = isEmEditText.getText().toString();
                password = isPEditText.getText().toString();
                con_password = isCnEdtiText.getText().toString().trim();
                String Text = school.getSelectedItem().toString();
                int nameId = school.getSelectedItemPosition();
                if(nameId > 0){
                    // Notify the selected item text
                    schoolId = list_data.get(nameId-1).get("school_id");
                }else {
                    alertDialog("Please select school");
                }if (email.length() <= 0
                        && password.length() <= 0) {
                    alertDialog("Please enter Email id and Password");
                } else if (email.length() <= 0) {
                    alertDialog("Please enter Email id");
                } else if (!isPasswordValid(password)) {
                    alertDialog("Password is to shot");
                } else if (password.length() <= 0) {
                    alertDialog("Please enter Password");
                } else if (!password.equals(con_password)) {
                    alertDialog("Password do not match");
                } else if(name.length() <=0){
                    alertDialog("Please enter Name");
                } else if (validateEmail((email))) {
                    userRegister(email, password, schoolId,name);
                } else {
                    alertDialog("Please enter valid Email id");
                }
                break;
            default:
                break;
        }
    }

    private void getSchool() {
        final ProgressDialog dialog = new ProgressDialog(RegisterActivity.this);
        dialog.setTitle("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(RegisterActivity.this);
        String requestURL = "http://rslinfotech.in/ObjectiveExam/Ws/getAll_school.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(",", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                if (s != null) {
                    if (!s.startsWith("null")) {
                        try {
                            JSONObject jObj = new JSONObject(s);
                            if (jObj.getString("result").equals("success")) {
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                                if (jObj.getJSONArray("data").length() > 0) {
                                    schoolname_Array.add("Please select school");
                                    for (int a = 0; a < jObj.getJSONArray("data")
                                            .length(); a++) {
                                        JSONObject b = jObj.getJSONArray("data")
                                                .getJSONObject(a);
                                        data = new HashMap<String, String>();
                                        data.put("school_id", b.getString("school_id"));
                                        data.put("school_name",
                                                b.getString("school_name"));
                                        schoolname_Array.add(b.getString("school_name"));
                                        list_data.add(data);
                                    }
                                    // Initializing an ArrayAdapter
                                    adapter = new ArrayAdapter<String>(RegisterActivity.this,
                                            android.R.layout.simple_spinner_item, schoolname_Array){
                                        @Override
                                        public boolean isEnabled(int position){
                                            if(position == 0)
                                            {
                                                // Disable the first item from Spinner
                                                // First item will be use for hint
                                                return false;
                                            }
                                            else
                                            {
                                                return true;
                                            }
                                        }
                                        @Override
                                        public View getDropDownView(int position, View convertView,
                                                                    ViewGroup parent) {
                                            View view = super.getDropDownView(position, convertView, parent);
                                            TextView tv = (TextView) view;
                                            if(position == 0){
                                                // Set the hint text color gray
                                                tv.setTextColor(Color.GRAY);
                                            }
                                            else {
                                                tv.setTextColor(Color.BLACK);
                                            }
                                            return view;
                                        }
                                    };
                                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    school.setAdapter(adapter);
                                }
                            } else if (jObj.getString("result")
                                    .equals("failed")) {
                                alertDialog("No School List"
                                );
                                dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    } else {
                        dialog.dismiss();
                    }
                } else {
                    dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("VolleyError",""+volleyError);
            }
        });
        queue.add(stringRequest);
    }

    private void userRegister(final String user_email,final String user_pass,final String school_id,final String user_name) {
        final ProgressDialog dialog = new ProgressDialog(RegisterActivity.this);
        dialog.setTitle("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(RegisterActivity.this);
        String requestURL = "http://rslinfotech.in/ObjectiveExam/Ws/User_registertion.php";
        if (encodedImage.equals("")) {
            Bitmap bm = BitmapFactory.decodeResource(getResources(),R.drawable.ic_profile);
            encodedImage = encodeTobase64(bm);
           // return;
        }
        JSONObject obj = new JSONObject();
        try {
            obj.put("img", "" + encodedImage);
            obj.put("user_email", user_email);
            obj.put("user_pass", user_pass);
            obj.put("school_id", school_id);
            obj.put("name", user_name);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("JSONObject ", "" + obj.toString());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(",", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                if (s != null) {
                    if (!s.startsWith("null")) {
                        try {
                            JSONObject jObj = new JSONObject(s);
                            if (jObj.length() > 0) {
                                if (jObj.getString("result").equals("success")) {

                                    Log.d("onResponse ", "" + jObj.toString());
//                                    editor.putString("students_id",
//                                            jObj.getString("students_id"));
//                                    editor.putString("students_email",
//                                            jObj.getString("students_email"));
//                                    editor.putString("school_id",
//                                            jObj.getString("school_id"));
//                                    editor.commit();
                                    //alertDialog("Register Success");
                                    //call Select Subject  Activity
                                    Intent in = new Intent(RegisterActivity.this, LoginActivity.class);
                                    startActivity(in);
                                    finish();
                                    if (dialog.isShowing()) {
                                        dialog.dismiss();
                                    }
                                } else if (jObj.getString("result")
                                        .equals("Email id alread exist")) {
                                    dialog.dismiss();
                                    alertDialog("Email id alread exist");
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    } else {
                        dialog.dismiss();
                    }
                } else {
                    dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("VolleyError",""+volleyError);
            }
        })
            {
                @Override
                protected Map<String, String> getParams () {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("user_email", user_email);
                params.put("user_pass", user_pass);
                params.put("school_id", school_id);
                params.put("name", user_name);
                params.put("img", "" + encodedImage);
                return params;
            }

                @Override
                public Map<String, String> getHeaders ()throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
            };
            queue.add(stringRequest);
    }

    private void alertDialog(String errer) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                RegisterActivity.this);
        alertDialog.setTitle("OnlineExam");
        alertDialog.setMessage(errer);
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                    }
                });
        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                RegisterActivity.this.finish();
            }
        });
        alertDialog.show();
    }
}

