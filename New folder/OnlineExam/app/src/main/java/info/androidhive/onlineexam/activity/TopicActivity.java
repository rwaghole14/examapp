package info.androidhive.onlineexam.activity;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.onlineexam.R;
import org.json.JSONObject;
import java.util.ArrayList;
import info.androidhive.onlineexam.adapter.TopicRecyclerViewAdapter;
import info.androidhive.onlineexam.model.TopicItem;

/**
 * Created by admin on 11/7/2015.
 */
public class TopicActivity extends AppCompatActivity {
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private Toolbar mToolbar;
    private TopicRecyclerViewAdapter myRecyclerViewAdapter;
    private RecyclerView recList;
    ArrayList<TopicItem> list_data;
    private ConnectionDetector cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic_selection);
        sp = getSharedPreferences("ObjectiveExam", 0);
        editor = sp.edit();
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setTitle("TOPICS");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recList = (RecyclerView) findViewById(R.id.cardList);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        list_data = new ArrayList<TopicItem>();
        cd = new ConnectionDetector(getApplicationContext());
        if (cd.isConnectingToInternet()) {
                getAlltopic();
        } else {
            cd.showAlertDialog(TopicActivity.this, "No Internet Connection",
                    "You don't have internet connection.", false);
        }
    }
    private void getAlltopic() {
        final ProgressDialog dialog = new ProgressDialog(TopicActivity.this);
        dialog.setTitle("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(TopicActivity.this);
        String requestURL = "http://rslinfotech.in/ObjectiveExam/Ws/getAll_topic.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(",", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                if (s != null) {
                    if (!s.startsWith("null")) {
                        try {
                            JSONObject jObj = new JSONObject(s);
                            if (jObj.getString("result").equals("success")) {
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                                if (jObj.getJSONArray("data").length() > 0) {
                                    for (int a = 0; a < jObj.getJSONArray("data")
                                            .length(); a++) {
                                        JSONObject b = jObj.getJSONArray("data")
                                                .getJSONObject(a);
                                        TopicItem data = new TopicItem();
                                        data.setTopicId(b.getString("topic_id"));
                                        data.setTopicName(b.getString("topic_name"));
                                        list_data.add(data);
                                    }
                                    myRecyclerViewAdapter = new TopicRecyclerViewAdapter(TopicActivity.this, list_data);
                                    recList.setAdapter(myRecyclerViewAdapter);
                                }
                            } else if (jObj.getString("result")
                                    .equals("failed")) {
                                // alertDialog(jObj.getString("No School List"));
                                dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    } else {
                        dialog.dismiss();
                    }
                } else {
                    dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
            }
        });
        queue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
