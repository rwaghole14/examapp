package info.androidhive.onlineexam.model;

/**
 * Created by admin on 11/7/2015.
 */
public class TopicItem
{
    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    private String topicId;
    private String topicName;

    public TopicItem(){

    }
    public TopicItem(String topicName, String topicId) {
        this.topicName = topicName;
        this.topicId = topicId;
    }
}
