package info.androidhive.onlineexam.activity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rsl.onlineexam.R;

import java.io.InputStream;
import java.net.URL;

import info.androidhive.onlineexam.model.RoundImage;


public class MeFragment extends Fragment
{
    // TODO: Rename parameter arguments, choose names that match
    private SharedPreferences sp;
    SharedPreferences.Editor editor;
    private TextView questions_done;
    private TextView best_topic;
    private ImageView user_image;
    private Button logout;
    private  RoundImage roundedImage;
    private  Bitmap bitmap;


    public MeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rooView= inflater.inflate(R.layout.fragment_me, container, false);
        sp = getActivity().getSharedPreferences("ObjectiveExam", 0);
        editor = sp.edit();
        MainActivity.mToolbar.setTitle(sp.getString("student_name",""));
        questions_done = (TextView)rooView.findViewById(R.id.question_done);
        best_topic = (TextView)rooView.findViewById(R.id.best_topic);
        user_image = (ImageView)rooView.findViewById(R.id.imageView1);
        if(sp.getString("student_image","").equals("")){
            Log.e("Student image if", sp.getString("student_image", ""));
            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_profile);
            roundedImage = new RoundImage(bm);
            user_image.setImageDrawable(roundedImage);
        }else {
            String pimg = "http://rslinfotech.in/ObjectiveExam/Ws/"
                    + sp.getString("student_image","");
            Log.e("Student image else", pimg);
            //Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_profile);
            new LoadImage().execute(pimg);
        }
        return  rooView;
    }
    private class LoadImage extends AsyncTask<String, String, Bitmap> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream)new URL(args[0]).getContent());

            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {

            if(image != null){
                roundedImage = new RoundImage(image);
                user_image.setImageDrawable(roundedImage);

            }else{
                Toast.makeText(getActivity(), "Image Does Not exist or Network Error", Toast.LENGTH_SHORT).show();

            }
        }
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }
    @Override
    public void onDetach() {
        super.onDetach();
    }
}
