package info.androidhive.onlineexam.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rsl.onlineexam.R;

import java.util.List;

import info.androidhive.onlineexam.model.TopicItem;

/**
 * Created by admin on 11/7/2015.
 */
public class TopicRecyclerViewAdapter extends RecyclerView.Adapter<TopicRecyclerViewAdapter.ItemHolder>
{
    private LayoutInflater layoutInflater;
    private Context context;
    TopicItem item;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private List<TopicItem> topicList;

    public TopicRecyclerViewAdapter(Context context, List<TopicItem> topicList) {
        sp = context.getSharedPreferences("ObjectiveExam", 0);
        editor = sp.edit();
        this.context = context;
        this.topicList = topicList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public ItemHolder onCreateViewHolder(
            ViewGroup viewGroup, int i) {

        CardView itemCardView = (CardView) layoutInflater.inflate(
                R.layout.topic_cardview, viewGroup, false);
        return new ItemHolder(itemCardView, this);
    }
    @Override
    public void onBindViewHolder(ItemHolder itemHolder,
                                 int i) {
        itemHolder.Subjectname.setText(topicList.get(i).getTopicName());
        //itemHolder.count.setText(""+videoFileName.length);
        itemHolder.setClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position) {
               // editor.putString("school_name",subjectList.get(position).getSubjectName());
               // editor.putString("school_id", subjectList.get(position).getSubjectId());
               // editor.commit();
               // Intent in=new Intent(context, MainActivity.class);
               // in.putExtra("school_name",subjectList.get(position).getSubjectName());
               // in.putExtra("school_id", subjectList.get(position).getSubjectId());
               // context.startActivity(in);
            }
        });
    }

    @Override
    public int getItemCount() {
        return topicList.size();
    }

    public void add(int location, String iName) {

        notifyItemInserted(location);
    }

    public void remove(int location) {
        if (location >= topicList.size())
            return;
        notifyItemRemoved(location);
    }

    public static class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TopicRecyclerViewAdapter parent;
        private CardView cardView;
        private ItemClickListener clickListener;
        TextView Subjectname;
        ImageView Image_display;

        public ItemHolder(CardView cView, TopicRecyclerViewAdapter parent) {
            super(cView);
            cardView = cView;
            this.parent = parent;
            Subjectname = (TextView) cardView.findViewById(R.id.subject_name);
            cView.setOnClickListener(this);
        }

        public void setClickListener(ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View v) {
            clickListener.onClick(v, getPosition());
        }
    }// class ItemHolder

    public interface ItemClickListener {
        void onClick(View view, int position);
    }
}
